import Vue from 'vue'
import Router from 'vue-router'
import Register3 from '@/components/Register3'
import AddGoods from '@/components/AddGoods'
import Goods from '@/components/Goods'
import Order from '@/components/Order'
import Success from '@/components/Success'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/register',
      name: 'Register3',
      component: Register3
    },
    {
      path: '/add_goods',
      name: 'AddGoods',
      component: AddGoods
    },
    {
      path: '/goods',
      name: 'Goods',
      component: Goods
    },
    {
      path: '/order',
      name: 'Order',
      component: Order
    },
    {
      path: '/success',
      name: 'Success',
      component: Success
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})
