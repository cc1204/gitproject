// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.axios = axios;
/* eslint-disable no-new */



// router.beforeEach((to, from, next) => {
  // to  从哪里来
  // from  要去哪里
  // next()  放行    next('/login)  强制你去/login
  // if (to.path == '/login') {
  //   next();
  // }
  // let auth = localStorage.getItem('auth');
  // if (!auth) {
  //   next('/login')
  // } else {
  //   next();
  // }

  // if (to.path == '/login') next();
  // let auth = localStorage.getItem('auth');
  // if (!auth) next('/login');
  // next();

// })



// 路由导航守卫  他的主要作用是什么？
// 每次页面跳转的时候，判断用户是否登陆过？如果没有登录的话，重定向到登录页。


router.beforeEach((to,from,next)=>{
  // 如果说我现在在的地方是登陆页面的话，我就应该直接放行。
  // 如果你不在登录页面的话，如果你没有登录过的话，我们直接让你去登录页。
  // 如果你登录过的话，我让你继续执行下去
  // 当前的路由  to.path
  if(to.path=='/login') next();
  if(!localStorage.getItem("auth")) next('/login');
  next();
})



new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
