from rest_framework import serializers
from . import models



# 注册
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        fields = '__all__'

# 分类
class CateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Category
        fields = '__all__'


# 商品
class GoodsSerializer(serializers.ModelSerializer):
    catename = serializers.CharField(source='cate.name',read_only=True)
    class Meta:
        model = models.Goods
        fields = '__all__'


# 订单
class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Order
        fields = '__all__'

class OrderSerializer1(serializers.ModelSerializer):
    goodname = serializers.CharField(source='good.name')
    price = serializers.SerializerMethodField()
    class Meta:
        model = models.Order
        fields = ('orderno','num','price','goodname')

    def get_price(self,obj):
        return obj.good.price