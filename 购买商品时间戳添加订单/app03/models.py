from django.db import models

# 用户
class User(models.Model):
    username = models.CharField(max_length=32,unique=True)
    password = models.CharField(max_length=255)
    auth = models.IntegerField(default=1)

    class Meta:
        db_table = 'user3'




# 商品分类
class Category(models.Model):
    name = models.CharField(max_length=32,unique=True)

    class Meta:
        db_table = 'category3'




# 商品
class Goods(models.Model):
    name = models.CharField(max_length=32,unique=True)
    price = models.DecimalField(max_digits=9,decimal_places=2)
    img = models.ImageField(upload_to='img')
    cate = models.ForeignKey('Category',on_delete=models.CASCADE)
    class Meta:
        db_table = 'goods3'


# 订单
class Order(models.Model):
    orderno = models.CharField(max_length=32)
    good = models.ForeignKey('Goods',on_delete=models.CASCADE)
    user = models.ForeignKey('User',on_delete=models.CASCADE)
    num = models.IntegerField()

    class Meta:
        db_table = 'order3'

