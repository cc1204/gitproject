from django.urls import path,include
from . import views
urlpatterns = [
    path('register/',views.RegisterView.as_view()),
    path('cate/',views.CategoryView.as_view()),
    path('goods/',views.GoodsView.as_view()),
    path('order/',views.OrderView.as_view()),
    path('login/',views.LoginView.as_view()),
]
