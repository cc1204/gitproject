from django.utils.deprecation import MiddlewareMixin

import json

from django.http import JsonResponse



#  用户登录
class CountLoginTimes(MiddlewareMixin):

    def process_request(self,request):

        # 在/app03/order/ 的 POST 请求下才执行
        #  当 auth 为 1 的时候，提示不能购买
        if request.method=='POST' and request.path == '/app03/order/':
            jsonStr = request.body.decode()
            dict = json.loads(jsonStr)
            auth = int(dict.get("auth",0))
            if auth == 1:
                return JsonResponse({"code":2000,"msg":"不能购买商品"})
