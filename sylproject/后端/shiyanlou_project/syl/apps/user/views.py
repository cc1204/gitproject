from django.shortcuts import render
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
# Create your views here.
from rest_framework import viewsets
from rest_framework.authentication import BasicAuthentication,SessionAuthentication
from rest_framework.decorators import action
from rest_framework.filters import OrderingFilter
from rest_framework.permissions import AllowAny,IsAdminUser,IsAuthenticated,IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.throttling import UserRateThrottle
from rest_framework.pagination import PageNumberPagination
from rest_framework.views import APIView
from rest_framework.permissions import BasePermission,SAFE_METHODS
from django.contrib.auth.hashers import check_password
from rest_framework_jwt.views import JSONWebTokenAPIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from user.models import User,UserToken
from user.serializers import UserSerializer
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired
from django.conf import settings



serializer = Serializer(settings.SECRET_KEY,600)
user_token = Serializer(settings.SECRET_KEY)


def index(request):
    # 需要认证才能访问的视图
    return HttpResponse('hello')



# 分页（局部）: 自定义分液器，局部
class PageNum(PageNumberPagination):
    # 查询字符串中代表每页返回数据量的参数名，默认值：None
    page_size_query_param = 'page_size'
    # 查询字符串中代表页码的参数名，有默认值：page
    # page_query_param = 'page'
    # 一页中最多的结果条数
    max_page_size = 2


# 自定义权限(局部）
class MyPermission(BasePermission):
    def has_permission(self,request,view):
        print(view.kwargs.get('pk'), request.user.id)
        '''判断用户对模型有没有访问权限'''
        # 任何用户对使用此类权限的视图都有访问权限
        print(request)
        if request.user.is_superuser:
            # 管理员对用户模型有访问权限
            return True
        elif view.kwargs.get('pk') == str(request.user.id):
            # 携带的id和用户的id相同时有访问权限
            return True
        return False

    def has_object_permission(self, request, view, obj):

        '''获取单个数据时，判断用户对某个数据对象时否有访问权限'''
        if request.user.id == obj.id:
            return True
        return False


class UserViewSet(viewsets.ModelViewSet):
    """
    完成产品的增删改查
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer  # 优先使用 get_serializer_class 返回的序列化器

    # # 1.认证： 自定义认证类，自定义会覆盖全局配置
    # authentication_classes = (BasicAuthentication,SessionAuthentication)
    # # 2.权限认证： 自定义权限类
    # permission_classes = (MyPermission)

    # 3.分页： 自定义分页器  覆盖全局配置
    pagination_class = PageNum


    # 4.限流：自定义限流类
    throttle_classes = [UserRateThrottle]

    # 5. 过滤： 指定过滤方法类，拍下方法类，一个或多个
    filter_backends = (DjangoFilterBackend, OrderingFilter)  # 同时支持过滤和排序
    # 5.1 指定排序字段，不设置，排序功能不起效
    ordering_fileds = ('date_joined', 'id')  # ?ordering = -id
    # 5.2指定过过滤字段，不设置，过滤功能不起效
    filter_fields = ('username', 'phone', 'is_active')  # ?username = tom&phone=is_active=tur

    # # 根据不同的请求，获得不同的序列化器
    # def get_serializer_class(self):
    #     if self.action == 'unactived':
    #         return UserUnActiveSerializer
    #     else:
    #         return UserSerializer

from django.http import JsonResponse
import json
# def login(request):
#     body_dict = json.loads(request.body)
#     print(body_dict,"----------------")
#     name = body_dict.get("name")
#     pwd = body_dict.get("pwd")
#     if not all([name,pwd]):
#         resp = {
#             "code":1001,
#             "msg":"信息不全"
#         }
#         return JsonResponse(resp)
#     if name == "zhangsan" and pwd == "123123":
#         resp = {
#             "code":0,
#             "msg":"登陆成功",
#             "data":{
#                 "id":1,
#                 "name":"张三",
#                 "age":18
#             }
#         }
#         return JsonResponse(resp)
#     return  JsonResponse({
#         "code":1002,
#         "msg":"验证失败"
#     })

from user.serializers import UserInfoSerializer
class UserInfoViewSet(APIView):
 # 查询用户信息
     def get(self, request, *args, **kwargs):
         # 一对多、多对多查询都是一样的语法
         obj = User.objects.all()
         ser = UserInfoSerializer(instance=obj,many=True) # 关联数据多个
         # ser = UserInfoSerializer(instance=obj[0]) # 关联数据一个
         return Response(ser.data, status=200)

     # 创建用户
     '''创建用户'''
     def post(self,request):
         ser = UserInfoSerializer(data=request.data)
         # 判断提交数据是否合法
         if ser.is_valid():
             ser.save()
             return Response(data=ser.data, status=201)
         return Response(data=ser.errors,status=400)

     # 更新用户信息
     def put(self, request):
         pk = request.query_params.get('pk')
         try:
             userinfo = User.objects.get(id=pk)
         except Exception as e:
             return Response(data='用户不存在', status=201)
         # 创建序列化对象，并将要反序列化的数据传递给data构造参数，进而进行验证
         ser = UserInfoSerializer(userinfo,data=request.data)
         if ser.is_valid():
             ser.save()
             return Response(data=ser.data, status=201)
         return Response(data=ser.errors,status=400)
     #  删除用户信息
     def delete(self,request):
         pk = request.query_params.get('pk')
         try:
            userinfo = User.objects.get(id=pk)
            userinfo.delete()
            return Response(data='删除成功', status=200)
         except Exception  as e:
             return Response(data='用户不存在',status=400)


from django_redis import get_redis_connection
'''自定义认证和权限优先级更高，可以覆盖settings.py中的 '''
# 自定义权限类
permission_classes = (MyPermission,)
# 自定义认证类, 自定义会覆盖全局配置
authentication_classes = (JSONWebTokenAuthentication,)

import datetime
import random
class RegisterView(APIView):
    """
    用户注册, 权限是: 匿名用户可访问
    """
    # 自定义权限类
    permission_classes = (AllowAny,)

    def post(self, request):
        """
        接收邮箱和密码, 前端校验两遍一致性, 注册成功后返回成功, 然后用户自行登录获取token
        1. 随机用户名
        2. 生成用户
        3. 设置用户密码
        4. 保存用户
        :param request:
        :return: {'code':0,'msg':'注册成功'}
        """
        username = request.data.get('username')
        phone = request.data.get('phone')
        code = request.data.get('code')
        passwrod = request.data.get('password')

        if all([username, passwrod, phone, code]):
            pass
        else:
            return Response({'code': 999, 'msg': '参数不全'})

        # rand_name = self.randomUsername()
        # 验证手机验证码

        redis_client = get_redis_connection('verify_code')
        code_redis = redis_client.get(phone)
        if code_redis:
            code_redis = code_redis.decode()

        if not code == code_redis:
            return Response({'code': 999, 'msg': '手机验证码错误'})

        user = User(username=username, phone=phone)
        user.set_password(passwrod)
        user.save()

        return Response({'code': 0, 'msg': '注册成功'})
    def randomUsername(self):
        """ 生成随机用户名: 格式: SYL + 年月日时分 + 5位随机数
        :return:
        """
        d = datetime.datetime.now()
        base = 'SYL'
        time_str = '%04d%02d%02d%02d%02d' % (d.year, d.month, d.day, d.hour, d.minute)
        rand_num = str(random.randint(10000, 99999))
        return base + time_str + rand_num


# class LoginView(APIView):
#     def post(self,request):
#         print(request.data)
#         email = request.data.get('email')
#         passwrod = request.data.get('password')
#         user_obj = User.objects.filter(email=email).first()
#         if user_obj:
#             if check_password(passwrod,user_obj.password):
#                 user_info = {'user_id':user_obj.pk}
#                 token = user_token.dumps(user_info).decode()
#                 # 把生成的token存起来
#                 UserToken.objects.update_or_create(user=user_obj,defaults={'token':token})
#                 return Response({'msg':'登陆成功','code':200,'token':token})
#             else:
#                 return Response({'msg':'邮箱或密码不正确','code':400})
#         else:
#             return Response({'msg':'邮箱或密码不正确','code':400})





        # email = request.data.get('email')
        # passwrod = request.data.get('password')
        # if all([email, passwrod]):
        #     pass
        # else:
        #     return Response({'code':9999,'msg':'参数不全'})
        # user_obj = User.objects.filter(email=email).first()
        # if user_obj:
        #     return Response({'msg':'登陆成功','code':200})
        # else:
        #     return Response({'msg':'邮箱或密码错误','code':400})

# 查询用户数量接口
class RegCountView(APIView):
    # 注册时需要验证的用户名和手机号是否使用

    # 自定义权限类
    permission_classes = (AllowAny,)

    def post(self, request):
        # 接收参数:  验证的内容type: username/phone,  data: '用户名' 或者 '手机号',
        global count
        datatype = request.data.get('type')
        data = request.data.get('data')
        if not all([data, datatype]):
            return Response({'code': 999, 'msg': '参数不完整'})
        if datatype == 'username':
            count = User.objects.filter(username=data).count()
        if datatype == 'phone':
            count = User.objects.filter(phone=data).count()
        return Response({'code': 0, 'msg': '查询成功', 'data': {'type': datatype, 'count': count}})