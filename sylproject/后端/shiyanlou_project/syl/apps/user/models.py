from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
from course.models import Base

from rest_framework.pagination import PageNumberPagination


# 创建Vip表，并和用户进行关联
class Vip(Base):
    vip_choise = (
        ('0', '普通用户'),
        ('1', '普通会员'),
        ('2', '高级会员'),
    )
    title = models.CharField('vip名称', max_length=16)
    vip_type = models.CharField('Vip种类', choices=vip_choise, max_length=4)
    desc = models.CharField('vip描述', max_length=255)
    period = models.IntegerField('有效期', default=365)

    class Meta:
        db_table = 'tb_vip'

    def __str__(self):
        return self.title


class User(AbstractUser):
    phone = models.CharField('手机号',max_length=20)
    img = models.ImageField(upload_to='user',null=True)
    nick_name = models.CharField('昵称',max_length=20)
    address = models.CharField('地址',max_length=255)

    vip = models.ForeignKey(Vip, on_delete=models.SET_NULL, default=None, null=True)
    vip_expiration = models.DateField('vip到期时间', blank=True, default=None, null=True)

    class Meta:
        db_table = 'tb_user'

class UserToken(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    token=models.CharField(max_length=300,verbose_name='用户token值')
    class Meta:
        db_table='user_token'
    def __str__(self):
        return self.token


