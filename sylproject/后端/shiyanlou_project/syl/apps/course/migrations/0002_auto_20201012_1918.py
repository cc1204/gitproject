# Generated by Django 2.2 on 2020-10-12 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('course', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sections',
            name='seq_num',
            field=models.IntegerField(default=1, verbose_name='序号'),
        ),
        migrations.AddField(
            model_name='sections',
            name='video',
            field=models.FileField(blank=True, max_length=1024, upload_to='videos/%y%m%d/', verbose_name='上传视频'),
        ),
    ]
