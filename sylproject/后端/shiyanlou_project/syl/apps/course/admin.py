from django.contrib import admin
from course.models import *
# Register your models here.

model_list =[CourseType,CourseTag,Course,Chapters,Sections,JieDuan,Path]
for model in model_list:
    admin.site.register(model)