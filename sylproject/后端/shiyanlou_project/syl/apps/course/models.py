from django.db import models


# Create your models here.


class Base(models.Model):
    create_time = models.TimeField(auto_now_add=True)
    update_time = models.TimeField(auto_now=True)

    class Meta:
        abstract = True


class CourseType(Base):
    title = models.CharField('课程类别', max_length=16)
    sequence = models.IntegerField('展示顺序', default=18)

    class Meta:
        db_table = 'tb_coursetype'

    def __str__(self):
        return self.title


class CourseTag(Base):
    title = models.CharField('课程标签', max_length=16)
    sequence = models.IntegerField('展示顺序', default=18)

    class Meta:
        db_table = 'tb_coursetag'

    def __str__(self):
        return self.title


class Course(Base):
    STATUS = (
        ('0', '即将上线'),
        ('1', '已上线'),
        ('2', '已下线'),
    )

    title = models.CharField('课程名称', max_length=24)
    desc = models.CharField('课程描述', max_length=256)
    img = models.ImageField('课程logo', upload_to='course', null=True)
    course_type = models.ForeignKey(CourseType, verbose_name='课程类型', on_delete=models.SET_NULL, default=None, null=True)
    course_tag = models.ManyToManyField(CourseTag, verbose_name='课程标签')
    status = models.CharField('课程状态', choices=STATUS, max_length=8, default='1')
    attention = models.IntegerField('关注人数', default=0)
    learner = models.IntegerField('学习人数', default=0)

    class Meta:
        db_table = 'tb_course'

    def __str__(self):
        return self.title


class Chapters(Base):
    title = models.CharField('章标题', max_length=24)
    serial_num = models.IntegerField('章序号')
    course = models.ForeignKey(Course, related_name='chapters', on_delete=models.SET_NULL, null=True)

    class Meta:
        db_table = 'tb_chapters'

    def __str__(self):
        return self.title


class Sections(Base):
    title = models.CharField('节标题', max_length=24)
    serial_num = models.CharField('节序号',max_length=52)
    chapters = models.ForeignKey(Chapters, related_name='sections', on_delete=models.SET_NULL, null=True)
    learn_time = models.CharField('学习小时',max_length=52)
    video = models.CharField("上传视频",max_length=1024)
    seq_num = models.CharField("序号",max_length=32)

    class Meta:
        db_table = 'tb_sections'

    def __str__(self):
        return self.title

# 路径表
class Path(Base):
    title = models.CharField('路径名', max_length=16)
    img = models.ImageField('路径图片', upload_to='path', null=True)
    desc = models.CharField('路径描述', max_length=255)
    user = models.ManyToManyField('user.User', blank=True)

    def course_total(self):
        count = 0
        for jd in self.jieduan.all():
            count = count + jd.courses.count()
        return count
    class Meta:
        db_table = 'tb_path'
    def __str__(self):
            return self.title

class JieDuan(Base):
    title = models.CharField('阶段名', max_length=16)
    serial_num = models.IntegerField('阶段序号')
    path = models.ForeignKey(Path, related_name='jieduan', on_delete=models.SET_NULL, null=True)
    courses = models.ManyToManyField(Course, blank=True)

    class Meta:
        db_table = 'tb_jieduan'
    def __str__(self):
        return "%s-第%s阶段-%s" % (self.path.title, self.serial_num, self.title)

class UserCourse(Base):
    """
    用户购买的课程
    """
    user = models.ForeignKey('user.User', on_delete=models.CASCADE,
                             related_name='paycourse')
    course = models.ForeignKey('course.Course', on_delete=models.CASCADE, related_name='payuser')

    class Meta:
        db_table = 'tb_usercourse'
    def __str__(self):
        return "用户:%s, 课程:%s" % (self.user.username, self.course.title)