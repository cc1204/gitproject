import Vue from 'vue'
import Router from 'vue-router'
import index from '@/components/index'
import paths from '@/components/paths'
import path_show from '@/components/path_show'
import bootcamp from '@/components/bootcamp'
import courses from '@/components/courses'
import course_show from '@/components/course_show'
import vip from '@/components/vip'
import questions from '@/components/questions'
import question_show from '@/components/question_show'
import privacy from '@/components/privacy'
import testcom from '@/components/testcom'
import user from '@/components/user'
import user_profile from '@/components/user_profile'
import tripalogin from '@/components/tripalogin'
import section_show from '@/components/section_show'
import users from '@/components/users'
import coupon from '@/components/coupon'
import web_shell from '@/components/web_shell'
import search from '@/components/search'
import oauth from '@/components/oauth'
import payment from '@/components/payment'
import play_video from '@/components/play_video'
import learn from '@/components/learn'
import learn2 from '@/components/learn2'
import comment_test from '@/components/comment_test'
import qn_upload from '@/components/QnUpload'

Vue.use(Router)

// 路由拦截
var check_login = (to, from, next) => {
    // 判断是否登录
    if (localStorage.getItem("uid")) {
        next()
    } else {
        alert("尚未登录，请先登录")
        next("/")
    }
};

var routes = [
    //
    { path: '/', name: 'index', component: index,},
    { path: '/qn_upload/', name: 'qn_upload', component: qn_upload,},


    {
        path: '/comment_test/',
        name: 'comment_test',
        component: comment_test
    },
    // 学习vue页面
    {
        path: '/learn2/',
        name: 'learn2',
        component: learn2
    },
    // 学习vue页面
    {
        path: '/learn/',
        name: 'learn',
        component: learn
    },
    // 视频播放页面
    {
        path: '/courses/course_play/',
        name: 'course_play',
        component: play_video
    },
    // 支付宝, 回调页面
    {
        path: '/payment/callback/',
        name: 'payment',
        component: payment
    },
    // weibo, 回调页面
    {
        path: '/oauth/callback/',
        name: 'oauth',
        component: oauth
    },
    // 领取优惠券
    {
        path: '/coupon',
        name: 'coupon',
        component: coupon
    },
    {
        path: '/section_show',
        name: 'section_show',
        component: section_show
    },
    {
        path: '/user_profile',
        name: 'user_profile',
        component: user_profile
    },
    {
        path: '/user',
        name: 'user',
        component: user,
        beforeEnter: check_login,
    },
    {
        path: '/testcom',
        name: 'testcom',
        component: testcom
    },
    {
        path: '/privacy',
        name: 'privacy',
        component: privacy
    },
    {
        path: '/questions/question_show',
        name: 'question_show',
        component: question_show
    },
    {
        path: '/questions',
        name: 'questions',
        component: questions
    },
    {
        path: '/vip',
        name: 'vip',
        component: vip
    },
    {
        path: '/courses/course_show',
        name: 'course_show',
        component: course_show
    },
    {
        path: '/courses',
        name: 'courses',
        component: courses
    },
    {
        path: '/bootcamp',
        name: 'bootcamp',
        component: bootcamp
    },
    {
        path: '/paths/path_show',
        name: 'path_show',
        component: path_show
    },
    {
        path: '/paths',
        name: 'paths',
        component: paths
    },
    {
        path: '/tripalogin',
        name: 'tripalogin',
        component: tripalogin,
    },
    {
        path: '/users',
        name: 'users',
        component: users,
    },
    {
        path: '/web_shell',
        name: 'web_shell',
        component: web_shell,
    },
    {
        path: '/search',
        name: 'search',
        component: search,
    },

]

export default new Router({
    routes: routes,
    mode: 'history' /*hash,history*/
})
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
