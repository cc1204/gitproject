from django.db import models


# Create your models here.

class User(models.Model):
    """
    用户表
        用户名、密码、手机号、头像
    """
    username = models.CharField(max_length=30)
    password = models.CharField(max_length=128)
    phone = models.CharField(max_length=11)
    icon = models.ImageField(upload_to='img')

    class Meta:
        db_table = 'user'

    def __str__(self):
        return self.username


class UserInfo(models.Model):
    """
    用户信息表
        邮箱、性别、年龄、家庭住址、详细住址，关联用户表
    """
    SEX_CHOICE = (
        (0, '男'),
        (1, '女')
    )
    email = models.EmailField(max_length=40)
    sex = models.IntegerField(choices=SEX_CHOICE, default=0)
    age = models.IntegerField()
    addr = models.CharField(max_length=200)
    detail = models.CharField(max_length=100)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'userinfo'

    def __str__(self):
        return '关联用户:{}'.format(self.user.username)


class UserToken(models.Model):
    """
    jwt:json web token
    """
    token = models.CharField(max_length=300)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = 'usertoken'

    def __str__(self):
        return self.token


"""
上下级关系非常明确
省市区县镇村
"""


class AreaInfo(models.Model):
    """
    自关联
    """
    name = models.CharField(max_length=50)
    pid = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)

    class Meta:
        db_table = 'areainfo'

    def __str__(self):
        return self.name
