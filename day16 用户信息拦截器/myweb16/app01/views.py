from django.contrib.auth.hashers import make_password, check_password
from django.conf import settings

from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
# Create your views here.
from app01.models import User, UserInfo, UserToken, AreaInfo
from app01.serializer import UserModelSerializer, UserInfoModelSerializer, AreaInfoModelSerializer
from itsdangerous import TimedJSONWebSignatureSerializer
import re

# 用于用户登陆加密使用
token_serializer = TimedJSONWebSignatureSerializer(settings.SECRET_KEY, 86400)


class RegisterView(APIView):
    """
    注册用户信息
        用户名、密码、确认密码、手机、头像
    """

    def post(self, request):
        # 获取网页提交数据
        username = request.data.get('username')
        password1 = request.data.get('password1')
        password2 = request.data.get('password2')
        phone = request.data.get('phone')
        icon = request.data.get('icon')
        #  获取数据库中的用户对象
        user_obj = User.objects.filter(username=username)
        # 判断
        if user_obj:
            # 当条件成立时，用户存在
            return Response({'msg': '用户存在', 'code': 400})
        # 使用re验证手机号
        if not re.match(r'^1[35678]\d{9}$', phone):
            return Response({'msg': '手机号不符号', 'code': 400})
        if password1 == password2:
            data = {
                'username': username, 'password': make_password(password1),
                'phone': phone, 'icon': icon
            }
            # 反序列化添加
            user_serializer = UserModelSerializer(data=data)
            if user_serializer.is_valid():
                # 校验通过
                user_serializer.save()
                # 返回
                return Response({'msg': '添加成功', 'code': 200})
            else:
                print(user_serializer.errors)
                return Response({'msg': '添加失败', 'code': 400})
        else:
            return Response({'msg': '两次密码不一致', 'code': 400})


class LoginView(APIView):
    """
    登录
    """

    def post(self, request):
        # 提取网页数据
        username = request.data.get('username')
        password = request.data.get('password')
        # 获取数据库中用户对象
        user_obj = User.objects.filter(username=username).first()
        # 判断
        if user_obj:
            if check_password(password, user_obj.password):
                user_info = {'user_id': user_obj.pk}
                token = token_serializer.dumps(user_info).decode()
                UserToken.objects.update_or_create(user=user_obj, defaults={'token': token})
                return Response({'msg': '登陆成功', 'code': 200, 'token': token, 'username': user_obj.username})
            else:
                return Response({'msg': '用户名或者密码不正确', 'code': 400})
        else:
            return Response({'msg': '用户名或者密码不正确', 'code': 400})


class UserInfoView(APIView):
    """
    判断用户信息是否录入
    """

    def get(self, request):
        token = request.GET.get('token')
        # 将token反序列化成字典
        user_info = token_serializer.loads(token)
        # 获取用户id
        user_id = user_info.get('user_id')
        # 获取用户信息对象
        userinfo_obj = UserInfo.objects.filter(user=user_id)
        if userinfo_obj:
            return Response({'msg': '用户信息完整', 'code': 200})
        else:
            return Response({'msg': '用户信息不完整', 'code': 400})

    def post(self, request):
        # 获取信息
        email = request.data.get('email')
        sex = request.data.get('sex')
        age = request.data.get('age')
        addr = request.data.get('addr')
        detail = request.data.get('detail')
        token = request.data.get('token')
        # 将token反序列化成字典
        user_info = token_serializer.loads(token)
        # 获取用户id
        user_id = user_info.get('user_id')
        data = {
            'email': email, 'sex': sex, 'age': age, 'addr': addr, 'detail': detail, 'user': user_id
        }
        userinfo_serializer = UserInfoModelSerializer(data=data)
        if userinfo_serializer.is_valid():
            userinfo_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(userinfo_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})


class AreaInfoView(APIView):
    """
    省市级联
        pid 默认为0，顶级的
    """

    def post(self, request):
        # 获取网页提交数据
        pid = request.data.get('pid')
        name = request.data.get('name')
        # 获取对象
        area_obj = AreaInfo.objects.filter(name=name)
        if pid == '0':
            if area_obj:
                return Response({'msg': '名称已存在', 'code': 400})
            else:
                data = {
                    'name': name,
                }
                area_serializer = AreaInfoModelSerializer(data=data)
                if area_serializer.is_valid():
                    # 校验通过，保存
                    area_serializer.save()
                    # 返回
                    return Response({'msg': '添加成功', 'code': 200})
                else:
                    print(area_serializer.errors)
                    return Response({'msg': '添加失败', 'code': 400})
        else:
            areainfo_obj = AreaInfo.objects.filter(pk=pid).first()
            if areainfo_obj:
                area_data = {
                    'name': name, 'pid': pid
                }
                area_ser = AreaInfoModelSerializer(data=area_data)
                if area_ser.is_valid():
                    area_ser.save()
                    return Response({'msg': '添加成功', 'code': 200})
                else:
                    return Response({'msg': '添加失败', 'code': 400})
            else:
                return Response({'msg': '上级分类不存在', 'code': 400})

    def get(self, request):
        # 获取省份
        province = AreaInfo.objects.filter(pid=None)
        # 序列化
        province_serializer = AreaInfoModelSerializer(province, many=True)
        # 返回数据
        return Response(province_serializer.data)


class CityView(APIView):
    def get(self, request, province_id):
        city_list = AreaInfo.objects.filter(pid=province_id)
        city_serializer = AreaInfoModelSerializer(city_list, many=True)
        return Response(city_serializer.data)
