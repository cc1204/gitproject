from rest_framework import serializers
from app01.models import User,UserInfo,AreaInfo

class UserModelSerializer(serializers.ModelSerializer):

    class Meta:
        model=User
        fields="__all__"


class UserInfoModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserInfo
        fields = "__all__"


class AreaInfoModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = AreaInfo
        fields = "__all__"
