import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Register from '@/components/Register'
import Login from '@/components/Login'
import Index from '@/components/Index'
import UserInfo from '@/components/UserInfo'
import Area from '@/components/Area'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/UserInfo',
      name: 'UserInfo',
      component: UserInfo,
      beforeEnter(to,from,next){
        // 声明token变量并从session中获取值
        let token=sessionStorage.getItem('token')
        // 判断token是否存在
        if(token){
          // token存在时，继续
          next()
        }else{
          //token不存在时，跳转到登陆
          next('/Login')
        }
      }
    },
    {
      path: '/Area',
      name: 'Area',
      component: Area
    },
  ]
})
