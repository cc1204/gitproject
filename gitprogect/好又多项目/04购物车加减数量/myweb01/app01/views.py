from django.contrib.auth.hashers import make_password, check_password
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.conf import settings
# Create your views here.
from app01.models import User, Cate, Picture, Goods, UserToken, SinaUser
from app01.serializer import UserModelSerializer, CateModelSerializer, PictureModelSerializer, GoodsModelSerializer
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired
import redis
import re
import requests

# 邮箱验证使用
serializer = Serializer(settings.SECRET_KEY, 600)

# 用户登陆使用
user_token = Serializer(settings.SECRET_KEY, 86400)

# 创建redis链接
r = redis.Redis(host='127.0.0.1', port=6379, db=6)

#删除购物车商品
class DelCart(APIView):
    def delete(self,request):
        token = request.data.get('token')
        user_info = user_token.loads(token)
        user_id = user_info.get('user_id')
        goods_id = request.data.get('goods_id')
        r.hdel(user_id,goods_id)
        return Response({'msg':'删除成功','code':200})



# 减数量
class MinusNum(APIView):
    def post(self, request):
        token = request.data.get('token')
        user_info = user_token.loads(token)
        user_id = user_info.get('user_id')
        goods_id = request.data.get('goods_id')
        goods_num = r.hget(user_id, goods_id).decode()  # 获取redis中商品的数量，用来判断商品数量大于1
        if int(goods_num) > 1:
            r.hincrby(user_id, goods_id, -1)
            return Response({'msg': 'OK', 'code': 200})
        else:
            return Response({'msg': '数量为0', 'code': 400})


# 加数量
class AddNum(APIView):
    def post(self, request):
        token = request.data.get('token')
        user_info = user_token.loads(token)
        user_id = user_info.get('user_id')
        goods_id = request.data.get('goods_id')
        r.hincrby(user_id, goods_id, 1)
        return Response({'msg': 'OK', 'code': 200})


# 获取购物车中的商品信息
class CartGoodsView(APIView):
    def get(self, request):
        token = request.GET.get('token')  # 获取网页提交数据
        if not token:
            return Response({'msg': '未登陆', 'code': 400})
        user_info = user_token.loads(token)  # 把token反序列化成字典
        user_id = user_info.get('user_id')  # 提取字典中的user_id
        # 通过用户的id,查找redis中所有的商品信息
        goods_list = []
        goods_dict = r.hgetall(user_id)  # hgetall获取到的内容是一个字典
        for k, v, in goods_dict.items():
            goods_obj = Goods.objects.get(pk=k.decode())
            goods_list.append({
                'id': goods_obj.pk,
                'goods_name': goods_obj.goods_name,
                'goods_img': goods_obj.goods_img.url,  # .url 提取图片链接
                'goods_price': goods_obj.goods_price,
                'goods_unit': goods_obj.goods_unit,
                'goods_num': v.decode(),
                'goods_sum': int(goods_obj.goods_price) * int(v.decode())
            })
        total = 0
        for i in goods_list:
            total += i.get('goods_sum')
        return Response({'msg': 'OK', 'code': 200, 'goods_info': goods_list, 'total': total})


# 获取redis中用户的购物车商品条数
class CartNumView(APIView):
    """
    # 获取购物车的商品条数
    """

    def get(self, request):
        token = request.GET.get('token')  # 获取网页提交数据
        user_info = user_token.loads(token)  # 把token反序列化成字典
        user_id = user_info.get('user_id')  # 提取字典中的user_id
        cart_num = len(r.hkeys(user_id))
        return Response({'msg': 'OK', 'code': 200, 'cart_num': cart_num})


class AddCartView(APIView):
    """
    添加购物车
    """

    def post(self, request):
        print(request.data)
        token = request.data.get('token')
        goods_num = request.data.get('goods_num')
        goods_id = request.data.get('goods_id')
        user_info = user_token.loads(token)
        # 提取用户 id
        user_id = user_info.get('user_id')
        print(user_info)
        # hincrby为key里面的字段加上数量
        r.hincrby(user_id, goods_id, goods_num)
        cart_num = len(r.hkeys(user_id))  # 那当前用户下所有的key并计算长度
        return Response({'msg': '添加成功', 'code': 200, 'cart_num': cart_num})


class CateView(APIView):
    # 获取分类
    def get(self, request):
        cate_list = Cate.objects.all()
        cate_serializer = CateModelSerializer(cate_list, many=True)
        return Response(cate_serializer.data)

    # 添加分类
    def post(self, request):
        print(request.data)
        cate_data = request.data
        cate_serializer = CateModelSerializer(data=cate_data)
        if cate_serializer.is_valid():
            cate_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            return Response({'msg': '添加失败', 'code': 400})

    # 修改分类
    def put(self, request):
        print(request.data)
        cate_info = Cate.objects.get(pk=request.data.get('cate_id'))
        cate_info.cate_name = request.data.get('cate_name')
        cate_info.cate_img = request.data.get('cate_img')
        cate_info.save()
        return Response({'msg': '修改成功', 'code': 200})

    # 删除数据
    def delete(self, request):
        print(request.data.get('cate_id'))
        # 获取网页提交数据
        cate_id = request.data.get('cate_id')
        # 获取数据库数据
        cate_obj = Cate.objects.get(pk=cate_id)
        if cate_obj:
            cate_obj.delete()
            return Response({'msg': '删除成功', 'code': 200})
        else:
            return Response({'msg': '分类不存在', 'code': 200})
        # 获取数据库数据


class PictureView(APIView):
    def get(self, request):
        # 获取所有的图片
        picture_list = Picture.objects.all()
        picture_ser = PictureModelSerializer(picture_list, many=True)
        return Response(picture_ser.data)


class GoodsView(APIView):
    """
    添加商品,需要参数
    """

    def post(self, request):
        print(request.data)

        goods_data = request.data
        goods_serializer = GoodsModelSerializer(data=goods_data)
        if goods_serializer.is_valid():
            goods_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            return Response({'msg': '添加失败', 'code': 400})

    def get(self, request):
        goods_list = Goods.objects.all()
        goods_serializer = GoodsModelSerializer(goods_list, many=True)
        return Response(goods_serializer.data)


# class GoodsShowPage(APIView):
#     """
#     商品分页,需要按分类进行,所以需要分类id作为参数
#     """
#     def get(self,request,cate_id,page_code):
#         cate_obj=Cate.objects.get(pk=cate_id)
#         goods_list=Goods.objects.filter(cate=cate_obj)
#         paginotor=Paginator(goods_list,10)
#         paged=paginotor.page(page_code)
#         goods_serializer=GoodsModelSerializer(paged,many=True)
#         return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':goods_serializer.data})

# class AddMarkdown(APIView):
#     def post(self,request):
#         print(request.data)
#         return Response({'msg':'OK'})

class RegisterView(APIView):
    def post(self, request):
        print(request.data)
        username = request.data.get('username')
        password1 = request.data.get('password1')
        password2 = request.data.get('password2')
        email = request.data.get('email')
        if not re.match(r'[0-9a-zA-Z_]{0,19}@(.*?).com', email):
            return Response({'msg': '邮箱不符合规则', 'code': 400})
        user_obj = User.objects.filter(username=username)
        if user_obj:
            return Response({'msg': '该用户已存在', 'code': 400})
        else:
            if password1 == password2:
                data = {
                    'username': username,
                    'password': make_password(password1),
                    'email': email
                }
                user_serializer = UserModelSerializer(data=data)
                if user_serializer.is_valid():
                    user_serializer.save()
                    user_info = {'user_id': request.data.get('id')}  # 把用户信息制作成字典
                    token = serializer.dumps(user_info).decode()  # 对用户信息进行序列化加密
                    subject = '好又多会员注册'
                    message = '欢迎注册好又多商场会员'
                    from_email = settings.EMAIL_FROM
                    recipient_list = [email]
                    html_message = '<h3>欢迎注册好又多好又多商场会员，请点击以下链接进行激活</br><a href="http://127.0.0.1:8000/app01/register_view/?token={}">激活会员请点击这里</a></h3>'.format(
                        token)
                    send_mail(subject=subject, message=message, from_email=from_email, recipient_list=recipient_list,
                              html_message=html_message)
                    return Response({'msg': '注册成功', 'code': 200})
                else:
                    return Response({'msg': '注册失败', 'code': 400})
            else:
                return Response({'msg': '两次输入密码不一致', 'code': 400})

    def get(self, request):
        print(request.GET.get('token'))
        token = request.GET.get('token')
        try:
            user_info = serializer.loads(token)
            print(user_info)
            return Response({'msg': 'OK', 'code': 200, 'token': user_info})
        except SignatureExpired:
            return Response({'msg': '激活失败', 'code': 400})


class LoginView(APIView):
    """
    登录需要参数:用户名、密码
    使用post方式提交数据
    """

    def post(self, request):
        print(request.data)
        username = request.data.get('username')
        password = request.data.get('password')
        # 获取数据库中的数据
        user_obj = User.objects.filter(username=username).first()
        if user_obj:
            if check_password(password, user_obj.password):
                user_info = {'user_id': user_obj.pk}
                token = user_token.dumps(user_info).decode()
                # 把生成的token存起来
                UserToken.objects.update_or_create(user=user_obj, defaults={'token': token})
                return Response({'msg': '登陆成功', 'code': 200, 'token': token})
            else:
                return Response({'msg': '用户名或密码不正确', 'code': 400})
        else:
            return Response({'msg': '用户名或密码不正确', 'code': 400})


class MicroBlogLogin(APIView):
    def post(self, request):
        code = request.data.get('code')
        print(code)
        # 请求参数
        sina_data = {
            'client_id': '1822670730',
            'client_secret': '02d1ac815a915ddfedf4767cb8966bdd',
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': 'http://127.0.0.1:8080/CallBack'
        }
        # 请求地址
        sina_link = 'https://api.weibo.com/oauth2/access_token'
        # 发送请求
        resp = requests.post(url=sina_link, data=sina_data)
        sina_info = resp.json()
        sina_uid = sina_info.get('uid')
        # 获取新浪用户
        sina_obj = SinaUser.objects.filter(sina_uid=sina_uid)
        if sina_obj:
            return Response({'msg': '授权登陆成功', 'code': 200, 'uid': sina_uid})
        else:
            SinaUser.objects.create(sina_uid=sina_uid)
            return Response({'msg': '新用户授权登陆成功', 'code': 200, 'uid': sina_uid})


class ShowDetailsView(APIView):
    def get(self, request, gid):
        goods_obj = Goods.objects.get(pk=gid)
        good_serializer = GoodsModelSerializer(goods_obj)
        return Response(good_serializer.data)
