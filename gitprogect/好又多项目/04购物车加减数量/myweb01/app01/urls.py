from django.urls import path
from .views import *

urlpatterns=[
   path('cate_view/',CateView.as_view()),
   path('picture_view/',PictureView.as_view()),
   path('goods_view/',GoodsView.as_view()),
   path('register_view/',RegisterView.as_view()),
   path('login_view/',LoginView.as_view()),
   path('micro_blog_login/',MicroBlogLogin.as_view()),  #新浪微博登陆
   path('show_details_view/<gid>',ShowDetailsView.as_view()),
   path('add_cart_view/',AddCartView.as_view()),
   path('cart_num_view/',CartNumView.as_view()),
   path('cart_goods_view/',CartGoodsView.as_view()),
   path('add_num/',AddNum.as_view()),  #购物车加数量
   path('minus_num/',MinusNum.as_view()),  #购物车减数量
   path('del_cart/',DelCart.as_view()),  #删除购物车
]