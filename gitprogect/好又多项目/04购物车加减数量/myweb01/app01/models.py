from django.db import models

# Create your models here.


"""
用户模块
    用户名、密码、邮箱
"""
class User(models.Model):
    username=models.CharField(max_length=40,verbose_name='用户名')
    password=models.CharField(max_length=128,verbose_name='密码')
    email=models.EmailField(max_length=60,blank=True,null=True,verbose_name='邮箱')
    class Meta:
        db_table='user'
    def __str__(self):
        return self.username


class UserToken(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    token=models.CharField(max_length=300,verbose_name='用户token值')
    class Meta:
        db_table='user_token'
    def __str__(self):
        return self.token

class SinaUser(models.Model):
    sina_uid=models.CharField(max_length=20)
    name=models.CharField(max_length=30,null=True,blank=True)
    first_date=models.DateTimeField(auto_now_add=True)
    login_date=models.DateTimeField(auto_now=True)
    class Meta:
        db_table='sina_user'
    def __str__(self):
        return self.sina_uid





class Cate(models.Model):
    """
    分类名称(cate_name)、图片（cate_img）
    """
    cate_name = models.CharField(max_length=40, verbose_name='分类名称')
    cate_img=models.ImageField(upload_to='cate',verbose_name='分类图片')
    class Meta:
        db_table='cate'
        verbose_name='商品分类'
        verbose_name_plural=verbose_name
    def __str__(self):
        return self.cate_name

# class Goods(models.Model):


"""

轮播图的批号:
    第一批

轮播图的图片
    批号:外键
    图片:

"""

class Carousel(models.Model):
    """
    轮播图号
    """
    carousel=models.CharField(max_length=20)

    def __str__(self):
        return self.carousel

    class Meta:
        db_table='carousel'

class Picture(models.Model):
    """
    轮播图的图片
    """
    carousel=models.ForeignKey(Carousel,on_delete=models.CASCADE)
    picture=models.ImageField(upload_to='picture')
    def __str__(self):
        return self.picture.url
    class Meta:
        db_table='picture'


class Goods(models.Model):
    goods_name=models.CharField(max_length=80,verbose_name='商品名称')
    goods_img=models.ImageField(upload_to='goods',verbose_name='商品图片')
    goods_price=models.DecimalField(max_digits=7,decimal_places=2,verbose_name='价格')
    goods_unit=models.CharField(max_length=8,verbose_name='单位')  #kg
    goods_describe=models.CharField(max_length=500,verbose_name='简介')
    goods_content=models.CharField(max_length=500,verbose_name='详情')
    cate=models.ForeignKey(Cate,on_delete=models.CASCADE,verbose_name='商品分类')
    class Meta:
        db_table='goods'
    def __str__(self):
        return self.goods_name

