from django.contrib import admin
from .models import *
# Register your models here.

#创建模型列表,把所有的model类放入里面，用来for循环使用，
# 后期新增的model类，都添加到model_list中就可以

model_list=[Cate,Picture,Carousel,Goods]

#循环model_list,用来在admin中注册模型
for model in model_list:
    admin.site.register(model)
