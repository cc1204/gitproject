import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Index from '@/components/Index'
import AddCate from '@/components/AddCate'
import AddGoods from '@/components/AddGoods'
import CallBack from '@/components/CallBack'
import Detail from '@/components/Detail'
import Cart from '@/components/Cart'
Vue.use(Router)


//导入manager文件夹下的内容

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/AddCate',
      name: 'AddCate',
      component: AddCate
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/AddGoods',
      name: 'AddGoods',
      component: AddGoods
    },
    {
      path: '/CallBack',
      name: 'CallBack',
      component: CallBack
    },
    {
      path: '/Detail/:gid',
      name: 'Detail',
      component: Detail
    },
    {
      path: '/Cart',
      name: 'Cart',
      component: Cart
    },
    
    

  
   
  ]
})
