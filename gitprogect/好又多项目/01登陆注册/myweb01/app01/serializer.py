from rest_framework import serializers
from app01.models import User,Cate,Picture

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields="__all__"

class CateModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Cate
        fields="__all__"

class PictureModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Picture
        fields="__all__"

# class CarouselModelSerializer(serializers.ModelSerializer):
#     class Meta:
#         model=Carousel
#         fields="__all__"