from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import User,Cate,Picture,Carousel
from app01.serializer import UserModelSerializer,CateModelSerializer,PictureModelSerializer

# class LoginView(APIView):
#     def post(self,request):
#         print(request.data)
#         user_obj=User.objects.filter(username=request.data.get('username')).first()
#         if user_obj:
#             if user_obj.password==request.data.get('password'):
#                 return Response({'msg':'登陆成功','code':200})
#             else:
#                 return Response({'msg': '用户名或密码不正确', 'code': 400})
#         else:
#             return Response({'msg': '用户名或密码不正确', 'code': 400})
#
# class ShowCateView(APIView):
#     def get(self,request):
#         cate_list=Cate.objects.all()
#         cate_serializer=CateModelSerializer(cate_list,many=True)
#         return Response(cate_serializer.data)
#
# class AddCateView(APIView):
#     def post(self,request):
#         print(request.data)
#         cate_data=request.data
#         cate_serializer=CateModelSerializer(data=cate_data)
#         if cate_serializer.is_valid():
#             cate_serializer.save()
#             return Response({'msg':'添加成功','code':200})
#         else:
#             return Response({'msg': '添加失败', 'code': 400})
#
# class ShowCateApi(APIView):
#     def get(self,request,cate_id):
#         cate_obj=Cate.objects.get(pk=cate_id)
#         cate_ser=CateModelSerializer(cate_obj)
#         return Response(cate_ser.data)
# class UpdateCateView(APIView):
#     def post(self,request):
#         cate_info=Cate.objects.get(pk=request.data.get('cate_id'))
#         cate_info.cate_name=request.data.get('cate_name')
#         cate_info.cate_img=request.data.get('cate_img')
#         cate_info.save()
#         return Response({'msg':'OK','code':200})
#
# class DeleteCateView(APIView):
#     def get(self,request,cid):
#         cate_obj=Cate.objects.get(pk=cid)
#         cate_obj.delete()
#         return Response({'msg':'删除成功','code':200})

class CateView(APIView):
    #获取分类
    def get(self, request):
        cate_list=Cate.objects.all()
        cate_serializer=CateModelSerializer(cate_list,many=True)
        return Response(cate_serializer.data)
    #添加分类
    def post(self, request):
        print(request.data)
        cate_data=request.data
        cate_serializer=CateModelSerializer(data=cate_data)
        if cate_serializer.is_valid():
            cate_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            return Response({'msg': '添加失败', 'code': 400})

    #修改分类
    def put(self, request):
        print(request.data)
        cate_info=Cate.objects.get(pk=request.data.get('cate_id'))
        cate_info.cate_name=request.data.get('cate_name')
        cate_info.cate_img=request.data.get('cate_img')
        cate_info.save()
        return Response({'msg':'修改成功','code':200})

    #删除数据
    def delete(self, request):
        print(request.data.get('cate_id'))
        #获取网页提交数据
        cate_id=request.data.get('cate_id')
        #获取数据库数据
        cate_obj=Cate.objects.get(pk=cate_id)
        if cate_obj:
            cate_obj.delete()
            return Response({'msg': '删除成功', 'code': 200})
        else:
            return Response({'msg': '分类不存在', 'code': 200})
        # 获取数据库数据


class PictureView(APIView):
    def get(self,request):
        #获取所有的图片
        picture_list=Picture.objects.all()
        picture_ser=PictureModelSerializer(picture_list,many=True)
        return Response(picture_ser.data)