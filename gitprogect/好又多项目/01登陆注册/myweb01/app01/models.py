from django.db import models

# Create your models here.

class User(models.Model):
    username=models.CharField(max_length=40)
    password=models.CharField(max_length=128)
    class Meta:
        db_table='user'
    def __str__(self):
        return self.username

class Cate(models.Model):
    """
    分类名称(cate_name)、图片（cate_img）
    """
    cate_name = models.CharField(max_length=40, verbose_name='分类名称')
    cate_img=models.ImageField(upload_to='cate',verbose_name='分类图片')
    class Meta:
        db_table='cate'
        verbose_name='商品分类'
        verbose_name_plural=verbose_name
    def __str__(self):
        return self.cate_name

# class Goods(models.Model):


"""

轮播图的批号:
    第一批

轮播图的图片
    批号:外键
    图片:

"""

class Carousel(models.Model):
    """
    轮播图号
    """
    carousel=models.CharField(max_length=20)

    def __str__(self):
        return self.carousel

    class Meta:
        db_table='carousel'

class Picture(models.Model):
    """
    轮播图的图片
    """
    carousel=models.ForeignKey(Carousel,on_delete=models.CASCADE)
    picture=models.ImageField(upload_to='picture')
    def __str__(self):
        return self.picture.url
    class Meta:
        db_table='picture'
