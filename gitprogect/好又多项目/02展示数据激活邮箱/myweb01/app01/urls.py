from django.urls import path
from .views import CateView,PictureView,GoodsView,RegisterView

urlpatterns=[
   path('cate_view/',CateView.as_view()),
   path('picture_view/',PictureView.as_view()),
   path('goods_view/',GoodsView.as_view()),
   path('register_view/',RegisterView.as_view()),
]