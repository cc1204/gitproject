from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.conf import settings
# Create your views here.
from app01.models import User,Cate,Picture,Goods
from app01.serializer import UserModelSerializer,CateModelSerializer,PictureModelSerializer,GoodsModelSerializer
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer, SignatureExpired

serializer=Serializer(settings.SECRET_KEY,60)
import re


class CateView(APIView):
    #获取分类
    def get(self, request):
        cate_list=Cate.objects.all()
        cate_serializer=CateModelSerializer(cate_list,many=True)
        return Response(cate_serializer.data)
    #添加分类
    def post(self, request):
        print(request.data)
        cate_data=request.data
        cate_serializer=CateModelSerializer(data=cate_data)
        if cate_serializer.is_valid():
            cate_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            return Response({'msg': '添加失败', 'code': 400})

    #修改分类
    def put(self, request):
        print(request.data)
        cate_info=Cate.objects.get(pk=request.data.get('cate_id'))
        cate_info.cate_name=request.data.get('cate_name')
        cate_info.cate_img=request.data.get('cate_img')
        cate_info.save()
        return Response({'msg':'修改成功','code':200})

    #删除数据
    def delete(self, request):
        print(request.data.get('cate_id'))
        #获取网页提交数据
        cate_id=request.data.get('cate_id')
        #获取数据库数据
        cate_obj=Cate.objects.get(pk=cate_id)
        if cate_obj:
            cate_obj.delete()
            return Response({'msg': '删除成功', 'code': 200})
        else:
            return Response({'msg': '分类不存在', 'code': 200})
        # 获取数据库数据


class PictureView(APIView):
    def get(self,request):
        #获取所有的图片
        picture_list=Picture.objects.all()
        picture_ser=PictureModelSerializer(picture_list,many=True)
        return Response(picture_ser.data)


class GoodsView(APIView):
    """
    添加商品,需要参数
    """
    def post(self,request):
        print(request.data)

        goods_data=request.data
        goods_serializer=GoodsModelSerializer(data=goods_data)
        if goods_serializer.is_valid():
            goods_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        goods_list=Goods.objects.all()
        goods_serializer=GoodsModelSerializer(goods_list,many=True)
        return Response(goods_serializer.data)

# class GoodsShowPage(APIView):
#     """
#     商品分页,需要按分类进行,所以需要分类id作为参数
#     """
#     def get(self,request,cate_id,page_code):
#         cate_obj=Cate.objects.get(pk=cate_id)
#         goods_list=Goods.objects.filter(cate=cate_obj)
#         paginotor=Paginator(goods_list,10)
#         paged=paginotor.page(page_code)
#         goods_serializer=GoodsModelSerializer(paged,many=True)
#         return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':goods_serializer.data})

# class AddMarkdown(APIView):
#     def post(self,request):
#         print(request.data)
#         return Response({'msg':'OK'})

class RegisterView(APIView):
    def post(self,request):
        print(request.data)
        username=request.data.get('username')
        password1=request.data.get('password1')
        password2=request.data.get('password2')
        email=request.data.get('email')
        if not re.match(r'[0-9a-zA-Z_]{0,19}@(.*?).com',email):
            return Response({'msg':'邮箱不符合规则','code':400})
        user_obj=User.objects.filter(username=username)
        if user_obj:
            return Response({'msg':'该用户已存在','code':400})
        else:
            if password1 == password2:
                data = {
                    'username': username,
                    'password': make_password(password1),
                    'email': email
                }
                user_serializer = UserModelSerializer(data=data)
                if user_serializer.is_valid():
                    user_serializer.save()
                    user_info={'user_id':request.data.get('id')}    #把用户信息制作成字典
                    token=serializer.dumps(user_info).decode()       #对用户信息进行序列化加密
                    subject='好又多会员注册'
                    message='欢迎注册好又多商场会员'
                    from_email=settings.EMAIL_FROM
                    recipient_list=[email]
                    html_message='<h3>欢迎注册好又多好又多商场会员，请点击以下链接进行激活</br><a href="http://127.0.0.1:8000/app01/register_view/?token={}">激活会员请点击这里</a></h3>'.format(token)
                    send_mail(subject=subject,message=message,from_email=from_email,recipient_list=recipient_list,html_message=html_message)
                    return Response({'msg': '注册成功', 'code': 200})
                else:
                    return Response({'msg': '注册失败', 'code': 400})
            else:
                return Response({'msg': '两次输入密码不一致', 'code': 400})

    def get(self,request):
        print(request.GET.get('token'))
        token=request.GET.get('token')
        try:
            user_info=serializer.loads(token)
            print(user_info)
            return Response({'msg':'OK','code':200,'token':user_info})
        except SignatureExpired:
            return Response({'msg':'激活失败','code':400})



