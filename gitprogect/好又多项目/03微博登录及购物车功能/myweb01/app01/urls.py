from django.urls import path
from .views import CateView,PictureView,GoodsView,RegisterView,LoginView,MicroBlogLogin,ShowDetailsView,AddCartView

urlpatterns=[
   path('cate_view/',CateView.as_view()),
   path('picture_view/',PictureView.as_view()),
   path('goods_view/',GoodsView.as_view()),
   path('register_view/',RegisterView.as_view()),
   path('login_view/',LoginView.as_view()),
   path('micro_blog_login/',MicroBlogLogin.as_view()),  #新浪微博登陆
   path('show_details_view/<gid>',ShowDetailsView.as_view()),  #新浪微博登陆
   path('add_cart_view/',AddCartView.as_view()),  #新浪微博登陆
]