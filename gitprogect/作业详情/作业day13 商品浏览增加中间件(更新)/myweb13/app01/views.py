from django.core.paginator import Paginator
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import Cate,Goods
from app01.serializer import CateModelSerializer,GoodsModelSerializer

class CateView(APIView):
    def post(self,request):
        print(request.data)
        if Cate.objects.filter(name=request.data.get('name')):
            return Response({'msg':'该分类已存在','code':400})
        cate_serializer=CateModelSerializer(data=request.data)
        if cate_serializer.is_valid():
            cate_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(cate_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        cate_list=Cate.objects.all()
        cate_serializer=CateModelSerializer(cate_list,many=True)
        return Response(cate_serializer.data)

class GoodsView(APIView):
    def post(self,request):
        print(request.data)
        if Goods.objects.filter(name=request.data.get('name')):
            return Response({'msg':'该商品已存在','code':400})
        goods_serializer=GoodsModelSerializer(data=request.data)
        if goods_serializer.is_valid():
            goods_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(goods_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        pid=request.GET.get('pid')
        goods_list=Goods.objects.all()
        paginotor=Paginator(goods_list,3)
        try:
            paged = paginotor.page(pid)
        except:
            paged = paginotor.page(1)
        goods_serializer=GoodsModelSerializer(paged,many=True)
        return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':goods_serializer.data})

class DetailsView(APIView):
    def get(self,request,gid):
        goods_obj=Goods.objects.get(pk=gid)
        goods_serializer=GoodsModelSerializer(goods_obj)
        return Response(goods_serializer.data)