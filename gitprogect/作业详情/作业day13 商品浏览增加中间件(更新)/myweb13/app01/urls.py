from django.urls import path
from .views import CateView,GoodsView,DetailsView
urlpatterns=[
    path('cate_view/',CateView.as_view()),
    path('goods_view/',GoodsView.as_view()),
    path('details_view/<gid>',DetailsView.as_view()),
]