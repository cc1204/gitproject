from django.utils.deprecation import MiddlewareMixin
from app01.models import Goods

class DetailsMiddleware(MiddlewareMixin):
    def process_request(self,request):
        print(request.path_info)
        current_path=request.path_info
        if current_path.startswith('/app01/details_view'):
            gid=current_path.split('/')[-1]
            goods_obj=Goods.objects.get(pk=gid)
            goods_obj.count+=1
            goods_obj.save()