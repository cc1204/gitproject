import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddCate from '@/components/AddCate'
import AddGoods from '@/components/AddGoods'
import ShowGoods from '@/components/ShowGoods'
import ShowDetails from '@/components/ShowDetails'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddCate',
      name: 'AddCate',
      component: AddCate
    },
    {
      path: '/AddGoods',
      name: 'AddGoods',
      component: AddGoods
    },
    {
      path: '/ShowGoods',
      name: 'ShowGoods',
      component: ShowGoods
    },
    {
      path: '/ShowDetails/:gid',
      name: 'ShowDetails',
      component: ShowDetails
    }
  ]
})