from django.db import models

# Create your models here.

class Cate(models.Model):
    name=models.CharField(max_length=32)
    class Meta:
        db_table='cate'
    def __str__(self):
        return self.name

class Goods(models.Model):
    name=models.CharField(max_length=32)
    price=models.DecimalField(max_digits=20,decimal_places=2)
    img=models.ImageField(upload_to='img')
    count=models.IntegerField(default=0)
    cate=models.ForeignKey(Cate,on_delete=models.CASCADE)

    class Meta:
        db_table='goods'
    def __str__(self):
        return self.name
