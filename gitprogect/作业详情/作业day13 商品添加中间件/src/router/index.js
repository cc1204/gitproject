import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddCate from '@/components/AddCate'
import AddGoods from '@/components/AddGoods'
import ShowGoods from '@/components/ShowGoods'
import ShowDeatils from '@/components/ShowDeatils'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddCate',
      name: 'AddCate',
      component: AddCate
    },
    {
      path: '/AddGoods',
      name: 'AddGoods',
      component: AddGoods
    },
    {
      path: '/ShowGoods',
      name: 'ShowGoods',
      component: ShowGoods
    },
    {
      path: '/ShowDeatils/:gid',
      name: 'ShowDeatils',
      component: ShowDeatils
    }
  ]
})
