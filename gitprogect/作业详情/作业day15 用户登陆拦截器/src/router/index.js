import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Register from '@/components/Register'
import Login from '@/components/Login'
import UserInfo from '@/components/UserInfo'
import Index from '@/components/Index'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/UserInfo',
      name: 'UserInfo',
      component: UserInfo,
      beforeEnter(to,from,next){
        let token=sessionStorage.getItem('token')
        if(token){
          next()
        }else{
          next('/Login')
        }

      }
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    }
  ]
})
