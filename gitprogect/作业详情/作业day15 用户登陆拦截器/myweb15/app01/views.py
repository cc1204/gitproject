import re

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import User,UserInfo
from app01.serializer import UserModelSerializer,UserInfoModelSerializer

#用户登录时使用
from django.conf import settings
from itsdangerous import TimedJSONWebSignatureSerializer
user_token=TimedJSONWebSignatureSerializer(settings.SECRET_KEY,86400)

class RegisterView(APIView):
    def post(self,request):
        print(request.data)
        user_obj=User.objects.filter(username=request.data.get('username'))
        if user_obj:
            return Response({'msg':'该用户已存在','code':400})
        phone=request.data.get('phone')
        if not re.match('^[1][3,4,5,7,8][0-9]{9}$',phone):
            return Response({'msg':'手机号不符合规范','code':400})
        user_serializer=UserModelSerializer(data=request.data)
        if user_serializer.is_valid():
            user_serializer.save()
            return Response({'msg': '注册成功', 'code': 200})
        else:
            print(user_serializer.errors)
            return Response({'msg': '注册失败', 'code': 400})

class LoginView(APIView):
    def post(self,request):
        username=request.data.get('username')
        password=request.data.get('password')
        user_obj=User.objects.filter(username=username).first()
        if user_obj:
            if user_obj.password==password:
                user_info={'user_id':user_obj.pk}
                token=user_token.dumps(user_info).decode()
                return Response({'msg':'登陆成功','code':200,'token':token})
            else:
                return Response({'msg': '用户名或密码错误', 'code': 400})
        else:
            return Response({'msg': '用户名或密码错误', 'code': 400})

class UserInfoView(APIView):
    def post(self,request):
        print(request.data)
        email=request.data.get('email')
        user_gender=request.data.get('user_gender')
        age=request.data.get('age')
        address=request.data.get('address')
        user=request.data.get('user')
        user_info=user_token.loads(user)
        user_id=user_info.get('user_id')
        data={
            'email':email,
            'user_gender':user_gender,
            'age':age,
            'address':address,
            'user':user_id
        }
        user_serializer=UserInfoModelSerializer(data=data)
        if user_serializer.is_valid():
            user_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(user_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})



    def get(self,request):
        token=request.GET.get('token')
        user_info=user_token.loads(token)
        user_id=user_info.get('user_id')
        user_obj=User.objects.get(pk=user_id)
        if UserInfo.objects.filter(user=user_obj):
            return Response({'code':200})
        else:
            return Response({'msg':'您的信息未完善','code':400})






