from django.urls import path
from .views import *
urlpatterns=[
    path('register_view/',RegisterView.as_view()),
    path('login_view/',LoginView.as_view()),
    path('userinfo_view/',UserInfoView.as_view()),
]