from django.db import models

# Create your models here.

class User(models.Model):
    username=models.CharField(max_length=32)
    password=models.CharField(max_length=128)
    phone=models.CharField(max_length=11)
    photo=models.ImageField(upload_to='img')
    class Meta:
        db_table='user'
    def __str__(self):
        return self.username

class UserInfo(models.Model):
    email=models.CharField(max_length=32)
    gender_choice=(('1','男'),('2','女'))
    user_gender=models.CharField(max_length=2,choices=gender_choice)
    age=models.IntegerField()
    address=models.CharField(max_length=100)
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    class Meta:
        db_table='userinfo'

