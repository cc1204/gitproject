from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import Department,User
from app01.serializer import DepartmentModelSerializer,UserModelSerializer

class DepartmentView(APIView):
    def post(self,request):
        print(request.data)
        if Department.objects.filter(name=request.data.get('name')):
            return Response({'msg':'该部门已存在','code':400})
        else:
            department_serializer=DepartmentModelSerializer(data=request.data)
            if department_serializer.is_valid():
                department_serializer.save()
                return Response({'msg': '添加成功', 'code': 200})
            else:
                print(department_serializer.errors)
                return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        department_list=Department.objects.all()
        department_serializer=DepartmentModelSerializer(department_list,many=True)
        return Response(department_serializer.data)

class UserView(APIView):
    def post(self,request):
        print(request.data)
        if User.objects.filter(username=request.data.get('username')):
            return Response({'msg':'该用户已存在','code':400})
        else:
            user_serializer=UserModelSerializer(data=request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                return Response({'msg': '添加成功', 'code': 200})
            else:
                print(user_serializer.errors)
                return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        pid=request.GET.get('pid')
        search_name=request.GET.get('search_name')
        user_list=User.objects.filter(Q(username__contains=search_name)|Q(department__name__contains=search_name)).all()
        paginotor=Paginator(user_list,5)
        paged=paginotor.page(pid)
        user_serializer=UserModelSerializer(paged,many=True)
        return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':user_serializer.data})

class LoginView(APIView):
    def post(self,request):
        user_obj=User.objects.filter(username=request.data.get('username')).first()
        if user_obj:
            if user_obj.password==request.data.get('password'):
                return Response({'msg':'登陆成功','code':200,'auth':user_obj.auth})
            else:
                return Response({'msg': '用户名或密码错误', 'code': 400})
        else:
            return Response({'msg': '用户名或密码错误', 'code': 400})



