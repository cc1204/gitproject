from django.db import models

# Create your models here.

class Department(models.Model):
    name=models.CharField(max_length=30)
    class Meta:
        db_table='department'

class User(models.Model):
    username=models.CharField(max_length=30)
    password=models.CharField(max_length=128)
    auth=models.IntegerField(default=1)
    department=models.ForeignKey(Department,on_delete=models.CASCADE)
    class Meta:
        db_table='user'

