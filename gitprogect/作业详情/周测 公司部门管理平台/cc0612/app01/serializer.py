from rest_framework import serializers
from app01.models import Department,User

class DepartmentModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Department
        fields="__all__"

class UserModelSerializer(serializers.ModelSerializer):
    department_name=serializers.CharField(source="department.name",default=True)
    class Meta:
        model=User
        fields="__all__"