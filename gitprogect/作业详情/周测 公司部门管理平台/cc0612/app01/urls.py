from django.urls import path
from .views import *

urlpatterns=[
    path('department_view/',DepartmentView.as_view()),
    path('user_view/',UserView.as_view()),
    path('login_view/',LoginView.as_view()),
]