import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddGrade from '@/components/AddGrade'
import UpdateGrade from '@/components/UpdateGrade'
import AddUser from '@/components/AddUser'
import ShowUser from '@/components/ShowUser'
import UpdateUser from '@/components/UpdateUser'
import AddCate from '@/components/AddCate'
import UpdateCate from '@/components/UpdateCate'
import AddStory from '@/components/AddStory'
import ShowStory from '@/components/ShowStory'
import UpdateStory from '@/components/UpdateStory'
import AddSection from '@/components/AddSection'
import ShowSection from '@/components/ShowSection'
import UpdateSection from '@/components/UpdateSection'
import ShowContent from '@/components/ShowContent'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddGrade',
      name: 'AddGrade',
      component: AddGrade
    },
    {
      path: '/UpdateGrade/:grade_id',
      name: 'UpdateGrade',
      component: UpdateGrade
    },
    {
      path: '/AddUser',
      name: 'AddUser',
      component: AddUser
    },
    {
      path: '/ShowUser',
      name: 'ShowUser',
      component: ShowUser
    },
    {
      path: '/UpdateUser/:user_id',
      name: 'UpdateUser',
      component: UpdateUser
    },
    {
      path: '/AddCate',
      name: 'AddCate',
      component: AddCate
    },
    {
      path: '/UpdateCate/:cate_id',
      name: 'UpdateCate',
      component: UpdateCate
    },
    {
      path: '/AddStory',
      name: 'AddStory',
      component: AddStory
    },
    {
      path: '/ShowStory/:cate_id',
      name: 'ShowStory',
      component: ShowStory
    },
    {
      path: '/UpdateStory',
      name: 'UpdateStory',
      component: UpdateStory
    },
    {
      path: '/AddSection',
      name: 'AddSection',
      component: AddSection
    },
    {
      path: '/ShowSection/:storys_id',
      name: 'ShowSection',
      component: ShowSection
    },
    {
      path: '/UpdateSection',
      name: 'UpdateSection',
      component: UpdateSection
    },
    {
      path: '/ShowContent/:sections_id',
      name: 'ShowContent',
      component: ShowContent
    },
  ]
})
