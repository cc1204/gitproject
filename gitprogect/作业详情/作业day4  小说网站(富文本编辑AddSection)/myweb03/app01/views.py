from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import  Response
import re
from django.conf import settings
# Create your views here.
from app01.models import Grade,User,Information,Author,Cate,Story,Section
from app01.serializer import GradeModelSerializer,UserModelSerializer,AuthorModelSerializer,InformationModelSerializer,CateModelSerializer,StoryModelSerializer,SectionModelSerializer
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,SignatureExpired

serializer=Serializer(settings.SECRET_KEY,60)

#等级表增删改查、

class GradeView(APIView):
    def post(self,request):
        print(request.data)
        grade_data=request.data
        grade_serializer=GradeModelSerializer(data=grade_data)
        if grade_serializer.is_valid():
            grade_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            print(grade_serializer.errors)
            return Response({'msg': '添加失败', 'code': 200})
    def get(self,request):
        grade_list=Grade.objects.all()
        grade_serializer=GradeModelSerializer(grade_list,many=True)
        return Response(grade_serializer.data)

class DeleteGradeView(APIView):
    def get(self,request,cid):
        grade_obj = Grade.objects.get(pk=cid)
        grade_obj.delete()
        return Response({'msg': '删除成功', 'code': 200})

class ShowGradeView(APIView):
    def get(self,request,grade_id):
        grade_obj=Grade.objects.get(pk=grade_id)
        grade_serializer=GradeModelSerializer(grade_obj)
        return Response(grade_serializer.data)

class UpdateGradeView(APIView):
    def post(self,request):
        grade_name=request.data.get('grade_name')
        grade_id=request.data.get('grade_id')
        grade_obj=Grade.objects.get(pk=grade_id)
        grade_obj.grade_name=grade_name
        grade_obj.save()
        return Response({'msg':'修改成功','code':200})

#用户表增删改查

class AddUserView(APIView):
    def post(self,request):
        print(request.data)
        username=request.data.get('username')
        password1=request.data.get('password1')
        password2=request.data.get('password2')
        user_email=request.data.get('user_email')
        if not re.match(r'[0-9a-zA-Z_]{0,19}@(.*?).com',user_email):
            return Response({'msg': '邮箱不符合规则', 'code': 400})
        user_obj=User.objects.filter(username=username)
        if user_obj:
            return Response({'msg':'该用户已存在','code':400})
        else:
            if password1==password2:
                data={
                    'username':username,
                    'password':make_password(password1),
                    'user_email':user_email,
                    'user_status':request.data.get('user_status')
                }
                user_serializer=UserModelSerializer(data=data)
                if user_serializer.is_valid():
                    user_serializer.save()
                    user_obj=User.objects.filter(username=username).first()
                    user_info={'user_id':user_obj.pk}
                    token=serializer.dumps(user_info).decode()
                    subject='微信读书会员注册'
                    message='欢迎注册微信读书商场会员'
                    from_email=settings.EMAIL_FROM
                    recipient_list=[user_email]
                    html_message='<h3>欢迎注册微信读书商场会员,请点击以下链接进行激活</br><a href="http://127.0.0.1:8000/app01/add_user_view/?token={}">激活会员请点击这里</a></h3>'.format(token)
                    send_mail(subject=subject,message=message,from_email=from_email,recipient_list=recipient_list,html_message=html_message)
                    return Response({'msg': '注册成功', 'code': 200})
                else:
                    return Response({'msg': '注册失败', 'code': 400})
            else:
                return Response({'msg': '两次密码输入不一致', 'code': 400})
    def get(self,request):
        print(request.GET.get('token'))
        token=request.GET.get('token')
        try:
            user_info=serializer.loads(token)
            print(user_info)
            return Response({'msg':'OK','code':200,'token':user_info})
        except SignatureExpired:
            return Response({'msg':'激活失败','code':400})

class ShowUserView(APIView):
    def get(self,request):
        user_list=User.objects.all()
        user_serializer=UserModelSerializer(user_list,many=True)
        return Response(user_serializer.data)

class DeleteUserView(APIView):
    def get(self,request,cid):
        user_obj=User.objects.get(pk=cid)
        user_obj.delete()
        return Response({'msg':'删除成功','code':200})

class UpdateUserView(APIView):
    def post(self,request):
        username=request.data.get('username')
        user_status=request.data.get('user_status')
        user_email=request.data.get('user_email')
        user_id=request.data.get('user_id')
        user_obj=User.objects.get(pk=user_id)
        user_obj.username=username
        user_obj.user_status=user_status
        user_obj.user_email=user_email
        user_obj.save()
        return Response({'msg':'修改成功','code':200})

class ShowUserDetailView(APIView):
    def get(self,request,user_id):
        user_list=User.objects.get(pk=user_id)
        user_serializer=UserModelSerializer(user_list)
        return Response(user_serializer.data)

#分类增删改查
class CateView(APIView):
    def post(self,request):
        print(request.data)
        cate_data=request.data
        cate_serializer=CateModelSerializer(data=cate_data)
        if cate_serializer.is_valid():
            cate_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            print(cate_serializer.errors)
            return Response({'msg': '添加失败', 'code': 200})
    def get(self,request):
        cate_list=Cate.objects.all()
        cate_serializer=CateModelSerializer(cate_list,many=True)
        return Response(cate_serializer.data)
class DeleteCateView(APIView):
    def get(self,request,cid):
        cate_obj = Cate.objects.get(pk=cid)
        cate_obj.delete()
        return Response({'msg': '删除成功', 'code': 200})

class ShowCateView(APIView):
    def get(self,request,cate_id):
        cate_obj=Cate.objects.get(pk=cate_id)
        cate_serializer=CateModelSerializer(cate_obj)
        return Response(cate_serializer.data)

class UpdateCateView(APIView):
    def post(self,request):
        cate_name=request.data.get('cate_name')
        cate_id=request.data.get('cate_id')
        cate_obj=Cate.objects.get(pk=cate_id)
        cate_obj.cate_name=cate_name
        cate_obj.save()
        return Response({'msg':'修改成功','code':200})

class StoryView(APIView):
    def post(self,request):
        print(request.data)
        story_data=request.data
        story_serializer=StoryModelSerializer(data=story_data)
        if story_serializer.is_valid():
            story_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            print(story_serializer.errors)
            return Response({'msg': '添加失败', 'code': 200})
    def get(self,request):
        story_list=Story.objects.all()
        story_serializer=StoryModelSerializer(story_list,many=True)
        return Response(story_serializer.data)

class DeleteStoryView(APIView):
    def get(self,request,cid):
        story_obj = Story.objects.get(pk=cid)
        story_obj.delete()
        return Response({'msg': '删除成功', 'code': 200})

class ShowStoryView(APIView):
    def get(self,request,story_id):
        story_obj=Story.objects.get(pk=story_id)
        story_serializer=StoryModelSerializer(story_obj)
        return Response(story_serializer.data)

class UpdateStoryView(APIView):
    def post(self,request):
        story_name=request.data.get('story_name')
        price=request.data.get('price')
        img=request.data.get('img')
        cate=request.data.get('cate')
        story_id=request.data.get('story_id')
        story_obj=Story.objects.get(pk=story_id)
        story_obj.story_name=story_name
        story_obj.price=price
        story_obj.img=img
        story_obj.story_name=story_name
        cate_obj=Cate.objects.get(pk=cate)
        story_obj.cate=cate_obj
        story_obj.save()
        return Response({'msg': '修改成功', 'code': 200})

class SectionView(APIView):
    def post(self,request):
        print(request.data)
        section_data=request.data
        section_serializer=SectionModelSerializer(data=section_data)
        if section_serializer.is_valid():
            section_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            print(section_serializer.errors)
            return Response({'msg': '添加失败', 'code': 200})
    def get(self,request):
        section_list=Section.objects.all()
        section_serializer=SectionModelSerializer(section_list,many=True)
        return Response(section_serializer.data)

class DeleteSectionView(APIView):
    def get(self,request,cid):
        section_obj = Section.objects.get(pk=cid)
        section_obj.delete()
        return Response({'msg': '删除成功', 'code': 200})

class ShowSectionView(APIView):
    def get(self,request,section_id):
        section_obj=Section.objects.get(pk=section_id)
        section_serializer=SectionModelSerializer(section_obj)
        return Response(section_serializer.data)

class UpdateSectionView(APIView):
    def post(self,request):
        story=request.data.get('story')
        section_name=request.data.get('section_name')
        content=request.data.get('content')
        section_id=request.data.get('section_id')
        section_obj=Section.objects.get(pk=section_id)
        section_obj.content=content
        section_obj.section_name=section_name
        story_obj=Story.objects.get(pk=story)
        section_obj.story=story_obj
        section_obj.save()
        return Response({'msg':'修改成功','code':200})

class ShowStoryDetailsView(APIView):
    def get(self,request,cate_id):
        cate_obj=Cate.objects.get(pk=cate_id)
        story_list=Story.objects.filter(cate=cate_obj)
        story_serializer=StoryModelSerializer(story_list,many=True)
        return Response(story_serializer.data)

class ShowSectionDetailsView(APIView):
    def get(self,request,storys_id):
        story_obj=Story.objects.get(pk=storys_id)
        section_list=Section.objects.filter(story=story_obj)
        section_serializer=SectionModelSerializer(section_list,many=True)
        return Response(section_serializer.data)

class ShowContentView(APIView):
    def get(self,request,sections_id):
        section_list=Section.objects.get(pk=sections_id)
        section_serializer=SectionModelSerializer(section_list)
        return Response(section_serializer.data)
