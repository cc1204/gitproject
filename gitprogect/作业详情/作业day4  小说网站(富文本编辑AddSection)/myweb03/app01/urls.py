from django.urls import path
from .views import *

urlpatterns=[
    path('grade_view/',GradeView.as_view()),
    path('delete_grade_view/<cid>',DeleteGradeView.as_view()),
    path('show_grade_view/<grade_id>',ShowGradeView.as_view()),
    path('update_grade_view/',UpdateGradeView.as_view()),
    path('add_user_view/',AddUserView.as_view()),
    path('show_user_view/',ShowUserView.as_view()),
    path('delete_user_view/<cid>',DeleteUserView.as_view()),
    path('update_user_view/',UpdateUserView.as_view()),
    path('show_user_detail_view/<user_id>',ShowUserDetailView.as_view()),
    path('cate_view/',CateView.as_view()),      #分类添加和展示
    path('delete_cate_view/<cid>',DeleteCateView.as_view()),      #分类删除接口
    path('show_cate_view/<cate_id>',ShowCateView.as_view()),      #分类删除接口
    path('update_cate_view/',UpdateCateView.as_view()),      #分类删除接口
    path('story_view/',StoryView.as_view()),      #小说添加展示接口
    path('delete_story_view/<cid>',DeleteStoryView.as_view()),      #小说删除接口
    path('show_story_view/<story_id>',ShowStoryView.as_view()),      #小说展示单条信息
    path('update_story_view/',UpdateStoryView.as_view()),      #小说修改接口
    path('section_view/',SectionView.as_view()),      #章节添加展示接口
    path('delete_section_view/<cid>',DeleteSectionView.as_view()),      #章节删除接口
    path('show_section_view/<section_id>',ShowSectionView.as_view()),      #章节展示单条
    path('update_section_view/',UpdateSectionView.as_view()),      #章节修改接口
    path('show_story_details_view/<cate_id>',ShowStoryDetailsView.as_view()),      #小说详情接口
    path('show_section_details_view/<storys_id>',ShowSectionDetailsView.as_view()),      #章节详情接口
    path('show_content_view/<sections_id>',ShowContentView.as_view()),      #内容详情接口
]