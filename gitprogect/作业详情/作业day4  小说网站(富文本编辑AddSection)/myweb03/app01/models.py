from django.db import models

# Create your models here.


#等级表
class Grade(models.Model):
    GRADE_CHOICE = (
        (1,'普通用户'),
        (2,'管理员'),
        (3,'VIP用户'),
    )
    grade_name=models.IntegerField(choices=GRADE_CHOICE,default=1)
#用户表
class User(models.Model):
    username=models.CharField(max_length=40,verbose_name='用户名')
    password=models.CharField(max_length=128,null=True,blank=True,verbose_name='密码')
    user_status=models.BooleanField(default=True,verbose_name='用户状态')
    user_email=models.EmailField(max_length=60,verbose_name='邮箱')
    class Meta:
        db_table='user'
    def __str__(self):
        return self.username

#用户信息表
class Information(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE,verbose_name='关联用户')
    true_name=models.CharField(max_length=40,verbose_name='真实姓名')
    phone=models.CharField(max_length=11,verbose_name='手机号')
    id_number=models.CharField(max_length=18,verbose_name='身份证号')
    qq_number=models.CharField(max_length=11,verbose_name='QQ号')
    house_address=models.CharField(max_length=40,verbose_name='家庭地址')
    class Meta:
        db_table='information'
    def __str__(self):
        return self.true_name

#作者表
class Author(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE,verbose_name='关联用户')
    author_name=models.CharField(max_length=40,verbose_name='作者名')
    author_status=models.BooleanField(default=True,verbose_name='作者状态')
    class Meta:
        db_table='author'
    def __str__(self):
        return self.author_name

#分类表
class Cate(models.Model):
    cate_name=models.CharField(max_length=30,verbose_name='分类名')

    class Meta:
        db_table='cate'
    def __str__(self):
        return self.cate_name

#小说表
class Story(models.Model):
    story_name=models.CharField(max_length=40,verbose_name='小说名')
    author=models.ForeignKey(Author,on_delete=models.CASCADE,verbose_name='关联作者',null=True,blank=True)
    price=models.DecimalField(max_digits=8,decimal_places=2,verbose_name='价格')
    add_time=models.DateTimeField(auto_now_add=True,verbose_name='发布日期')
    img=models.ImageField(upload_to='img',verbose_name='图片')
    reading_quantity=models.IntegerField(default=0,verbose_name='阅读量')
    cate=models.ForeignKey(Cate,on_delete=models.CASCADE,verbose_name='分类')

    class Meta:
        db_table = 'story'

    def __str__(self):
        return self.story_name

#章节表

class Section(models.Model):
    story=models.ForeignKey(Story,on_delete=models.CASCADE,verbose_name='小说')
    section_name=models.CharField(max_length=30,verbose_name='章节名')
    content=models.TextField(verbose_name='内容')
    class Meta:
        db_table = 'section'

    def __str__(self):
        return self.content













