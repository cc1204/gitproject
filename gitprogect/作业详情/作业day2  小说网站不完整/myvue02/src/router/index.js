import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Register from '@/components/Register'
import Login from '@/components/Login'
import AddStory from '@/components/AddStory'
import Index from '@/components/Index'
import Details from '@/components/Details'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Register',
      name: 'Register',
      component: Register
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/AddStory',
      name: 'AddStory',
      component: AddStory
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/Details/:story_id',
      name: 'Details',
      component: Details
    }
  ]
})
