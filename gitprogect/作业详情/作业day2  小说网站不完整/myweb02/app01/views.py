from django.core.paginator import Paginator
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
# Create your views here.
from app01.models import User, Story
from app01.serializer import UserModelSerializer, StoryModelSerializer


class RegisterView(APIView):
    def post(self, request):
        print(request.data)
        if User.objects.filter(username=request.data.get('username')):
            return Response({'msg': '该用户已存在', 'code': 200})
        user_data = request.data
        user_serializer = UserModelSerializer(data=user_data)
        if user_serializer.is_valid():
            user_serializer.save()
            return Response({'msg': '注册成功', 'code': 200})
        else:
            print(user_serializer.errors)
            return Response({'msg': '注册失败', 'code': 400})


class LoginView(APIView):
    def post(self, request):
        print(request.data)
        user_obj = User.objects.filter(username=request.data.get('username')).first()
        if user_obj:
            if user_obj.password == request.data.get('password'):
                return Response({'msg': '登录成功', 'code': 200, 'user_id': user_obj.pk})
            else:
                return Response({'msg': '用户名或密码不正确', 'code': 400})
        else:
            return Response({'msg': '用户名或密码不正确', 'code': 400})


class AddStoryView(APIView):
    def post(self, request):
        print(request.data)
        story_data = request.data
        story_serializer = StoryModelSerializer(data=story_data)
        if story_serializer.is_valid():
            story_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(story_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})


class ShowStoryApi(APIView):
    def get(self, request, user_id):
        user_obj = User.objects.get(pk=user_id)
        story_list = Story.objects.filter(user=user_obj).order_by('-add_time')
        story_serializer = StoryModelSerializer(story_list, many=True)
        return Response(story_serializer.data)


class DeleteStoryView(APIView):
    def get(self, request, cid):
        story_obj = Story.objects.get(id=cid)
        story_obj.delete()
        return Response({'msg': '删除成功', 'code': 200})


class ShowStoryPageView(APIView):
    def post(self, request):
        pid = request.data.get('pid')
        user_id = request.data.get('user_id')
        user_obj = User.objects.get(pk=user_id)
        story_list = Story.objects.filter(user=user_obj).order_by('-add_time')
        paginotor = Paginator(story_list, 3)
        paged = paginotor.page(pid)
        story_serializer = StoryModelSerializer(paged, many=True)
        return Response({'page_sum': paginotor.num_pages, 'page_list': [i for i in paginotor.page_range],
                         'data': story_serializer.data})


class ShowStoryDetailsView(APIView):
    def get(self, request, story_id):
        story_obj = Story.objects.get(pk=story_id)
        story_serializer = StoryModelSerializer(story_obj)
        return Response(story_serializer.data)
