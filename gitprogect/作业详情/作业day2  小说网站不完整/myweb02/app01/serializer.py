from rest_framework import serializers
from app01.models import User,Story

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields="__all__"

class StoryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Story
        fields="__all__"
