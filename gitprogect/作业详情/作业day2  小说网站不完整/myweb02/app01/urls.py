from django.urls import path
from .views import RegisterView,LoginView,AddStoryView,ShowStoryApi,DeleteStoryView,ShowStoryPageView,ShowStoryDetailsView

urlpatterns=[
    path('register_view/',RegisterView.as_view()),
    path('login_view/',LoginView.as_view()),
    path('add_story_view/',AddStoryView.as_view()),
    path('show_story_api/<user_id>',ShowStoryApi.as_view()),
    path('delete_story_view/<cid>',DeleteStoryView.as_view()),
    path('show_story_page_view/',ShowStoryPageView.as_view()),
    path('show_story_details_view/<story_id>',ShowStoryDetailsView.as_view()),
]