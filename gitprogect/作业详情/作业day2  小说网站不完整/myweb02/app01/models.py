from django.db import models

# Create your models here.

class User(models.Model):
    username=models.CharField(max_length=40,verbose_name='用户名')
    password=models.CharField(max_length=128,verbose_name='密码')
    class Meta:
        db_table='user'
    def __str__(self):
        return self.username

class Story(models.Model):
    story_name=models.CharField(max_length=100,blank=True,null=True,verbose_name='小说名称')
    user=models.ForeignKey(User,on_delete=models.CASCADE,verbose_name='小说作者')
    add_time=models.DateTimeField(auto_now=True,verbose_name='发布时间')
    img=models.ImageField(upload_to='img',verbose_name='小说图片')
    description=models.CharField(max_length=300,verbose_name='小说简述')
    class Meta:
        db_table='story'
    def __str__(self):
        return self.story_name


