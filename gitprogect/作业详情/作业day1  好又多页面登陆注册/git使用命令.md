## git基本操作

#### 新安装git以后,做全局配置

```
git config --global user.name ""
git config --global user.email ""
```

 第一次安装成功做的操作，以后不需要重复操作

#### git基本操作命令

 **1.初始化**

 git init

 **2.添加文件到缓存中**

 git add 文件名 #添加一个文件

 git add . # 添加全部文件

 **3.提交文件，并添加提交说明**

 git comment -m 'first comment'

 **4.指定origin的提交地址**

 git remote add origin <https://gitee.com/Stevezsq/h1910achechuan.git>

##### 5.推送文件到gitee仓库,并指定分支,master为主分支

 git push origin master

 **6.克隆远程内容**

```
git clone 远程地址
```

 git clone <https://gitee.com/Stevezsq/h1910achechuan.git> 克隆 github上的内容到本地

 **7.拉取gitee仓库中的内容，gitee仓库中代码有变化，本地没有变动**

 git pull <https://gitee.com/haiming0415/H1910A.g>