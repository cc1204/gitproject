from django.urls import path
from .views import CateView,PictureView

urlpatterns=[
   path('cate_view/',CateView.as_view()),
   path('picture_view/',PictureView.as_view()),
]