import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Index from '@/components/Index'
import Login from '@/components/Login'
import Reserve from '@/components/Reserve'
import ShowReverse from '@/components/ShowReverse'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Reserve/:product_id',
      name: 'Reserve',
      component: Reserve
    },
    {
      path: '/ShowReverse',
      name: 'ShowReverse',
      component: ShowReverse
    },
  ]
})
