from django.db import models

# Create your models here.

class Product(models.Model):
    img=models.ImageField(upload_to='img',verbose_name='图片')
    title=models.CharField(max_length=128,verbose_name='标题')
    price=models.DecimalField(max_digits=8,decimal_places=2,verbose_name='正常价格')
    vip_price=models.DecimalField(max_digits=8,decimal_places=2,verbose_name='会员价格')
    class Meta:
        db_table='product'

class User(models.Model):
    username=models.CharField(max_length=30,verbose_name='用户名')
    password=models.CharField(max_length=128,verbose_name='密码')
    class Meta:
        db_table='user'
    def __str__(self):
        return self.username

class Order(models.Model):
    uid=models.ForeignKey(User,on_delete=models.CASCADE,verbose_name='会员ID')
    title=models.CharField(max_length=128,verbose_name='商品标题')
    price=models.DecimalField(max_digits=8,decimal_places=2,verbose_name='价格')
    username=models.CharField(max_length=30,verbose_name='姓名')
    phone=models.CharField(max_length=11)
    class Meta:
        db_table='orders'