from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import Product,User,Order
from app01.serializer import ProductModelSerializer,UserModelSerializer,OrderModelSerializer,OrderSerializer
import re


class ShowProductView(APIView):
    def get(self,request):
        product_list=Product.objects.all()
        product_serializer=ProductModelSerializer(product_list,many=True)
        return Response(product_serializer.data)

class LoginView(APIView):
    def post(self,request):
        user_obj=User.objects.filter(username=request.data.get('username')).first()
        if user_obj:
            if user_obj.password==request.data.get('password'):
                return Response({'msg':'登陆成功','code':200,'user_id':user_obj.pk,'username':user_obj.username})
            else:
                return Response({'msg':'用户名或密码不正确','code':400})
        else:
            return Response({'msg': '用户名或密码不正确', 'code': 400})

class ShowDetailView(APIView):
    def get(self,request,product_id):
        product_list=Product.objects.get(pk=product_id)
        product_serializer=ProductModelSerializer(product_list)
        return Response(product_serializer.data)

class CommitView(APIView):
    def post(self,request):
        print(request.data)
        if not re.match(r"^1[356789]\d{9}$",request.data.get('phone')):
            return Response({'msg':'手机号不合法','code':400})
        user_obj=User.objects.filter(username=request.data.get('username')).first()
        if user_obj:
            order_serializer=OrderModelSerializer(data=request.data)
            if order_serializer.is_valid():
                order_serializer.save()
                return Response({'msg':'成功下单','code':200})
            else:
                print(order_serializer.errors)
                return Response({'msg': '下单失败', 'code': 400})

        else:
            return Response({'msg': '该用户不存在', 'code': 400})

class ShowReserveView(APIView):
    def get(self,request):
        user_obj=request.GET.get('user_id')
        reserve_list=Order.objects.filter(uid=user_obj)
        reserve_serializer=OrderSerializer(reserve_list,many=True)
        return Response(reserve_serializer.data)

