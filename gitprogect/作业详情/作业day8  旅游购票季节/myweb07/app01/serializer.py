from rest_framework import serializers
from app01.models import Product,User,Order

class ProductModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Product
        fields="__all__"

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields="__all__"

class OrderModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Order
        fields="__all__"

class OrderSerializer(serializers.Serializer):
    id=serializers.PrimaryKeyRelatedField(read_only=True)
    uid = serializers.PrimaryKeyRelatedField(read_only=True)
    title = serializers.CharField()
    price = serializers.CharField()
    username = serializers.CharField()
    phone = serializers.CharField()