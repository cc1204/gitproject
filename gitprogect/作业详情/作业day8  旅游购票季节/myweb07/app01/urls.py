from django.urls import path
from .views import *

urlpatterns=[
    path('show_product_view/',ShowProductView.as_view()),
    path('login_view/',LoginView.as_view()),
    path('show_detail_view/<product_id>',ShowDetailView.as_view()),
    path('commit_view/',CommitView.as_view()),
    path('show_reserve_view/',ShowReserveView.as_view()),
]