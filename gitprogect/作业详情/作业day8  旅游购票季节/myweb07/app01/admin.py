from django.contrib import admin
from app01.models import Product,User,Order
# Register your models here.

model_list=[Product,User,Order]

for model in model_list:
    admin.site.register(model)
