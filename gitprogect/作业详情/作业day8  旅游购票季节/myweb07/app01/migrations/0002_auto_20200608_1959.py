# Generated by Django 2.1.8 on 2020-06-08 19:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=8, verbose_name='价格'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.DecimalField(decimal_places=2, max_digits=8, verbose_name='正常价格'),
        ),
        migrations.AlterField(
            model_name='product',
            name='vip_price',
            field=models.DecimalField(decimal_places=2, max_digits=8, verbose_name='会员价格'),
        ),
    ]
