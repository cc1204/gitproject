import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myweb11.settings')
import django
django.setup()

from app02.models import Size,Goods

#添加尺寸
# Size.objects.create(size='165')
# Size.objects.create(size='170')
# Size.objects.create(size='175')
# Size.objects.create(size='180')

#添加商品
# size_id='3'
#
# goods_obj=Goods.objects.create(name='女式上衣',price="128.00")
# goods_obj.size.add(size_id)

#查询一件服装下面对应的尺寸
goods_obj=Goods.objects.get(pk=1)
print(goods_obj)
print(goods_obj.size.all())

#一个尺寸下对应的服装
size_obj=Size.objects.get(pk=3)
print(size_obj.goods_set.all())     #外键对象点儿类名小写_set.all()

