from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app02.models import Size,Goods
from app02.serializer import SizeModelSerializer,GoodsModelSerializer,GoodsSerializer

class SizeView(APIView):
    """
    添加尺寸
    """
    def post(self,request):
        name=request.data.get('size')
        if Size.objects.filter(size=name):
            return Response({'msg':'尺寸已存在','code':400})
        #反序列化添加数据
        size_serializer=SizeModelSerializer(data=request.data)
        if size_serializer.is_valid():
            size_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        #获取所有的尺寸
        size_list=Size.objects.all()
        #序列化
        size_serializer=SizeModelSerializer(size_list,many=True)
        return Response(size_serializer.data)

class GoodsView(APIView):
    def post(self,request):
        name = request.data.get('name')
        price = request.data.get('price')
        size = request.data.get('size')
        #将尺寸转换为列表
        size_list=[i for i in size if i !=',']
        if Goods.objects.filter(name=name):
            return Response({'msg': '该商品已存在', 'code': 400})
        # 反序列化添加数据
        data={
            'name':name,
            'size':size_list,
            'price':price
        }
        goods_serializer = GoodsModelSerializer(data=data)
        if goods_serializer.is_valid():
            goods_serializer.save()
            return Response({'msg': '添加成功', 'code': 200,'data':goods_serializer.data})
        else:
            print(goods_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400,'errors':goods_serializer.errors})
    def get(self,request):
        goods_list=Goods.objects.all()
        goods_serializer=GoodsSerializer(goods_list,many=True)
        return Response(goods_serializer.data)

