from django.db import models

# Create your models here.

class Size(models.Model):
    size=models.CharField(max_length=30)
    def __str__(self):
        return self.size

class Goods(models.Model):
    name=models.CharField(max_length=80)
    price=models.DecimalField(max_digits=9,decimal_places=2)
    size=models.ManyToManyField(Size)
    def __str__(self):
        return self.name

