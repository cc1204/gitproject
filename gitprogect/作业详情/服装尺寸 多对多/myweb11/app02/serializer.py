from rest_framework import serializers
from app02.models import Size,Goods

class SizeModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Size
        fields="__all__"

class GoodsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Goods
        fields="__all__"


class GoodsSerializer(serializers.Serializer):
    id=serializers.IntegerField()
    name = serializers.CharField()
    price = serializers.DecimalField(max_digits=9, decimal_places=2)
    size = serializers.SerializerMethodField()
    def get_size(self,obj):
        data=[ i.size for i in obj.size.all()]
        return '、'.join(data)