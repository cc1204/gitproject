from rest_framework import serializers
from app01.models import User,Travel_notes

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields="__all__"

class TravelNodesModelSerializer(serializers.ModelSerializer):
    user_name=serializers.CharField(source='user.username',default=True)
    photo=serializers.ImageField(source='user.photo',default=True)
    class Meta:
        model=Travel_notes
        fields="__all__"


