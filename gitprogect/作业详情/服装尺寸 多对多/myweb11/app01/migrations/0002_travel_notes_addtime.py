# Generated by Django 2.1.8 on 2020-06-13 04:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='travel_notes',
            name='addtime',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='发表时间'),
        ),
    ]
