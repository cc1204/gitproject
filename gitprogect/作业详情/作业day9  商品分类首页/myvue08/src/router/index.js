import Vue from 'vue'
import Router from 'vue-router'
import AddGoods from '@/components/AddGoods'
import Index from '@/components/Index'
Vue.use(Router)

export default new Router({
  routes: [
   
    {
      path: '/AddGoods',
      name: 'AddGoods',
      component: AddGoods
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    }
  ]
})
