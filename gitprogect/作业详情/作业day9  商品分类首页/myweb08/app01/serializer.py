from rest_framework import serializers
from app01.models import Cate,Goods

class CateModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Cate
        fields="__all__"

class GoodsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Goods
        fields="__all__"