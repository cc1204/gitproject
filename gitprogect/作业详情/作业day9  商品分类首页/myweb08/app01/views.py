from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import Cate,Goods
from app01.serializer import CateModelSerializer,GoodsModelSerializer

class AddGoodsView(APIView):
    def post(self,request):
        print(request.data)
        if Goods.objects.filter(goods_name=request.data.get('goods_name')):
            return Response({'msg':'该商品已存在','code':400})
        goods_serializer=GoodsModelSerializer(data=request.data)
        if goods_serializer.is_valid():
            goods_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            return Response({'msg': '添加失败', 'code': 200})

class ShowCateView(APIView):
    def get(self,request):
        cate_list=Cate.objects.all()
        cate_serializer=CateModelSerializer(cate_list,many=True)
        return Response(cate_serializer.data)

class ShowGoodsView(APIView):
    def get(self,request,pid):
        search_goods=request.GET.get('search_goods')
        cate_obj=int(request.GET.get('cate',0))
        if cate_obj:
            goods_list = Goods.objects.filter(cate=cate_obj).filter(Q(goods_name__contains=search_goods)|Q(cate__cate_name__contains=search_goods))
        else:
            goods_list = Goods.objects.filter(Q(goods_name__contains=search_goods)|Q(cate__cate_name__contains=search_goods)).all()
        paginotor=Paginator(goods_list,4)
        try:
            paged=paginotor.page(pid)
        except:
            paged = paginotor.page(1)
        goods_serializer=GoodsModelSerializer(paged,many=True)
        return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':goods_serializer.data})