from django.urls import path
from .views import AddGoodsView,ShowCateView,ShowGoodsView
urlpatterns=[
    path('add_goods_view/',AddGoodsView.as_view()),
    path('show_cate_view/',ShowCateView.as_view()),
    path('show_goods_view/<pid>',ShowGoodsView.as_view()),
]