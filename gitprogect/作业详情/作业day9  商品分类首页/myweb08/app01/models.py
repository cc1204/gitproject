from django.db import models

# Create your models here.

class Cate(models.Model):
    cate_name=models.CharField(max_length=30,verbose_name='分类名')
    class Meta:
        db_table='cate'
    def __str__(self):
        return self.cate_name

class Goods(models.Model):
    goods_name=models.CharField(max_length=40,verbose_name='商品名',null=True)
    price=models.DecimalField(max_digits=8,decimal_places=2,verbose_name='价格')
    img=models.ImageField(upload_to='img',verbose_name='图片',null=True)
    cate=models.ForeignKey(Cate,on_delete=models.CASCADE,verbose_name='分类')
    class Meta:
        db_table='goods'
    def __str__(self):
        return self.goods_name

