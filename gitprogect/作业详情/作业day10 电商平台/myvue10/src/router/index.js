import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import Index from '@/components/Index'
import MangerShop from '@/components/MangerShop'
import AddGoods from '@/components/AddGoods'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/MangerShop/:shop_id',
      name: 'MangerShop',
      component: MangerShop
    },
    {
      path: '/AddGoods/:shop_id',
      name: 'AddGoods',
      component: AddGoods
    },
  ]
})
