from django.db import models

# Create your models here.

#店主表
class Shop(models.Model):
    name=models.CharField(max_length=30)
    password=models.CharField(max_length=128,null=True)
    class Meta:
        db_table='shop'

#商品表
class Goods(models.Model):
    goods_name=models.CharField(max_length=30)
    goods_price=models.DecimalField(max_digits=8,decimal_places=2)
    img=models.ImageField(upload_to='img')
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    class Meta:
        db_table='goods'
