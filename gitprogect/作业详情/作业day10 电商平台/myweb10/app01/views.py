from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
# Create your views here.
from app01.models import Shop, Goods
from app01.serializer import ShopModelSerializer, GoodsModelSerializer


class LoginView(APIView):
    def post(self, request):
        print(request.data)
        user_obj = Shop.objects.filter(name=request.data.get('name')).first()
        if user_obj:
            if user_obj.password == request.data.get('password'):
                return Response({'msg': '登陆成功', 'code': 200, 'user_id': user_obj.pk})
            else:
                return Response({'msg': '用户名或密码错误', 'code': 400})
        else:
            return Response({'msg': '用户名或密码错误', 'code': 400})

    def get(self, request):
        user_list = Shop.objects.all()
        user_serializer = ShopModelSerializer(user_list, many=True)
        return Response(user_serializer.data)


class AddGoodsView(APIView):
    def post(self, request):
        print(request.data)
        if Goods.objects.filter(goods_name=request.data.get('goods_name')):
            return Response({'msg': '该商品已存在', 'code': 400})
        goods_serializer = GoodsModelSerializer(data=request.data)
        if goods_serializer.is_valid():
            goods_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(goods_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})


class ShowGoods(APIView):
    def get(self, request):
        shop_id = request.GET.get('shop_id')
        user_obj = Shop.objects.get(pk=shop_id)
        goods_list = Goods.objects.filter(shop=user_obj).order_by('-goods_price')
        goods_serializer = GoodsModelSerializer(goods_list, many=True)
        return Response(goods_serializer.data)

class ShowGoodsView(APIView):
    def get(self, request):
        goods_list = Goods.objects.all().order_by('-goods_price')
        goods_serializer = GoodsModelSerializer(goods_list, many=True)
        return Response(goods_serializer.data)

class DelGoods(APIView):
    def get(self, request, cid):
        goods_obj = Goods.objects.get(pk=cid)
        goods_obj.delete()
        return Response({'msg': '删除成功', 'code': 200})
