from rest_framework import serializers
from app01.models import Shop,Goods

class ShopModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Shop
        fields="__all__"

class GoodsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Goods
        fields="__all__"