from django.urls import path
from .views import *

urlpatterns=[
    path('login_view/',LoginView.as_view()),
    path('add_goods_view/',AddGoodsView.as_view()),
    path('show_goods/',ShowGoods.as_view()),
    path('del_goods/<cid>',DelGoods.as_view()),
    path('show_goods_view/',ShowGoodsView.as_view()),
]