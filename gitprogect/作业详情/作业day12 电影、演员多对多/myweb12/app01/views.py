from django.core.paginator import Paginator
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import Actor,Movie
from app01.serializer import ActorModelSerializer,MovieModelSerializer,MovieSerializer

class ActorView(APIView):
    def post(self,request):
        print(request.data)
        if Actor.objects.filter(actor=request.data.get('actor')):
            return Response({'msg':'该演员已存在','code':400})
        actor_serializer=ActorModelSerializer(data=request.data)
        if actor_serializer.is_valid():
            actor_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        actor_list=Actor.objects.all()
        actor_serializer=ActorModelSerializer(actor_list,many=True)
        return Response(actor_serializer.data)

class MovieView(APIView):
    def post(self,request):
        name=request.data.get('name')
        time=request.data.get('time')
        actor=request.data.get('actor')
        img=request.data.get('img')
        actor_list=[i for i in actor if i != ',']
        if Movie.objects.filter(name=name):
            return Response({'msg': '该电影已存在', 'code': 400})
        data={
            'name':name,
            'time':time,
            'actor':actor_list,
            'img':img
        }
        movie_serializer=MovieModelSerializer(data=data)
        if movie_serializer.is_valid():
            movie_serializer.save()
            return Response({'msg': '添加成功', 'code': 200,'data':movie_serializer.data})
        else:
            return Response({'msg': '添加失败', 'code': 400,'errors':movie_serializer.errors})
    def get(self,request):
        pid=request.GET.get('pid')
        movie_list=Movie.objects.all()
        paginotor=Paginator(movie_list,5)
        paged=paginotor.page(pid)
        movie_serializer=MovieSerializer(paged,many=True)
        return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':movie_serializer.data})


class ShowDetails(APIView):
    def get(self,request):
        mid=request.GET.get('mid')
        movie_list=Movie.objects.get(pk=mid)
        movie_serializer=MovieSerializer(movie_list)
        return Response(movie_serializer.data)
