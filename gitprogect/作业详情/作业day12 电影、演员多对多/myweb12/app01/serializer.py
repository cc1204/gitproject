from rest_framework import serializers
from app01.models import Actor,Movie

class ActorModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Actor
        fields="__all__"

class MovieModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Movie
        fields="__all__"

class MovieSerializer(serializers.Serializer):
    id=serializers.IntegerField()
    name = serializers.CharField()
    time = serializers.DateField()
    actor = serializers.SerializerMethodField()
    img = serializers.ImageField()
    def get_actor(self,obj):
        data=[i.actor for i in obj.actor.all()]
        return '、'.join(data)