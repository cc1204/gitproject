from django.db import models

# Create your models here.

class Actor(models.Model):
    actor=models.CharField(max_length=32)
    def __str__(self):
        return self.actor

class Movie(models.Model):
    name=models.CharField(max_length=52)
    time=models.DateField()
    actor=models.ManyToManyField(Actor)
    img=models.ImageField(upload_to='img')
    def __str__(self):
        return self.name