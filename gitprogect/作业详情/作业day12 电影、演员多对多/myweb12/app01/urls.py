from django.urls import path
from .views import ActorView,MovieView,ShowDetails

urlpatterns=[
    path('actor_view/',ActorView.as_view()),
    path('movie_view/',MovieView.as_view()),
    path('show_details/',ShowDetails.as_view()),
]