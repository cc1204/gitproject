import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddActor from '@/components/AddActor'
import AddMovie from '@/components/AddMovie'
import ShowMovie from '@/components/ShowMovie'
import ShowDetails from '@/components/ShowDetails'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddActor',
      name: 'AddActor',
      component: AddActor
    },
    {
      path: '/AddMovie',
      name: 'AddMovie',
      component: AddMovie
    },
    {
      path: '/ShowMovie',
      name: 'ShowMovie',
      component: ShowMovie
    },
    {
      path: '/ShowDetails/:mid',
      name: 'ShowDetails',
      component: ShowDetails
    }
  ]
})
