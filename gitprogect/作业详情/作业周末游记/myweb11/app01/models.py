from django.db import models


# Create your models here.


# 用户表(username,password,photo,signature)
class User(models.Model):
    username = models.CharField(max_length=30, verbose_name='姓名')
    password = models.CharField(max_length=128, verbose_name='密码')
    photo = models.ImageField(upload_to='photo', verbose_name='头像')
    signature = models.CharField(max_length=300, verbose_name='个性签名')

    class Meta:
        db_table = 'user'


# 游记表(title,picture,content,user外键,pageviews)
class Travel_notes(models.Model):
    title = models.CharField(max_length=300, verbose_name='标题')
    picture = models.ImageField(upload_to='picture', verbose_name='图片')
    content = models.TextField(verbose_name='内容')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='用户外键')
    pageviews = models.IntegerField(default=0, verbose_name='浏览次数')
    addtime = models.DateTimeField(auto_now_add=True, null=True, verbose_name='发表时间')

    class Meta:
        db_table = 'travel_notes'
