from django.urls import path
from .views import *

urlpatterns=[
    path('user_view/',UserView.as_view()),
    path('login_view/',LoginView.as_view()),
    path('travel_notes_view/',TravelnotesView.as_view()),
    path('show_tarvel_api/',ShowTarvelApi.as_view()),
]