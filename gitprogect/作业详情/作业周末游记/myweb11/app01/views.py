from django.core.paginator import Paginator
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import User,Travel_notes
from app01.serializer import UserModelSerializer,TravelNodesModelSerializer

#用户登录时使用
from django.conf import settings
from itsdangerous import TimedJSONWebSignatureSerializer
user_token=TimedJSONWebSignatureSerializer(settings.SECRET_KEY,86400)

#用户添加、查询
class UserView(APIView):
    def post(self,request):
        print(request.data)
        if User.objects.filter(username=request.data.get('username')):
            return Response({'msg':'该用户已存在','code':400})
        else:
            user_serializer=UserModelSerializer(data=request.data)
            if user_serializer.is_valid():
                user_serializer.save()
                return Response({'msg': '注册成功', 'code': 200})
            else:
                print(user_serializer.errors)
                return Response({'msg': '注册失败', 'code': 400})
    def get(self,request):
        token=request.GET.get('token')
        user_info=user_token.loads(token)
        user_id=user_info.get('user_id')
        user_list=User.objects.get(pk=user_id)
        user_serializer=UserModelSerializer(user_list)
        return Response(user_serializer.data)

#登录
class LoginView(APIView):
    def post(self,request):
        user_obj=User.objects.filter(username=request.data.get('username')).first()
        if user_obj:
            if user_obj.password==request.data.get('password'):
                user_info={'user_id':user_obj.pk}
                token=user_token.dumps(user_info).decode()
                return Response({'msg': '登录成功', 'code': 200,'token':token})
            else:
                return Response({'msg': '用户名或密码错误', 'code': 400})
        else:
            return Response({'msg': '用户名或密码错误', 'code': 400})

#添加游记、分页
class TravelnotesView(APIView):
    def post(self,request):
        print(request.data)
        title=request.data.get('title')
        content=request.data.get('content')
        img=request.data.get('picture')
        token=request.data.get('user')
        user_info=user_token.loads(token)
        user_id=user_info.get('user_id')
        data={
            'title':title,
            'content':content,
            'picture':img,
            'user':user_id
        }
        tarvel_serializer=TravelNodesModelSerializer(data=data)
        if tarvel_serializer.is_valid():
            tarvel_serializer.save()
            return Response({'msg': '发表成功', 'code': 200})
        else:
            print(tarvel_serializer.errors)
            return Response({'msg': '发表失败', 'code': 400})
    def get(self,request):
        pid=request.GET.get('pid')
        tarvel_list=Travel_notes.objects.all().order_by('-addtime')
        paginotor=Paginator(tarvel_list,5)
        paged=paginotor.page(pid)
        tarvel_serializer=TravelNodesModelSerializer(paged,many=True)
        return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':tarvel_serializer.data})

#添加浏览量
class ShowTarvelApi(APIView):
    def get(self,request):
        tid=request.GET.get('tid')
        travel_obj=Travel_notes.objects.get(pk=tid)
        travel_obj.pageviews += 1
        travel_obj.save()
        tarvel_serializer=TravelNodesModelSerializer(travel_obj)
        return Response(tarvel_serializer.data)