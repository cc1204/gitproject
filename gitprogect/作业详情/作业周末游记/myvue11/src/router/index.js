import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import Index from '@/components/Index'
import AddTravel from '@/components/AddTravel'
import ShowDetails from '@/components/ShowDetails'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/AddTravel',
      name: 'AddTravel',
      component: AddTravel
    },
    {
      path: '/ShowDetails/:tid',
      name: 'ShowDetails',
      component: ShowDetails
    }
  ]
})
