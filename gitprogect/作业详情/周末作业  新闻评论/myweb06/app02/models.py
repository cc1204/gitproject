from django.db import models

# Create your models here.

class Cate(models.Model):
    """
    小说分类
    """
    cate_name=models.CharField(max_length=30,verbose_name='分类名')
    link=models.CharField(max_length=200,verbose_name='链接')

class Fiction(models.Model):
    """
    小说
    """
    fic_name=models.CharField(max_length=80,verbose_name='小说名')
    author=models.CharField(max_length=30,verbose_name='作者')
    link=models.CharField(max_length=200,verbose_name='链接')
    cate=models.ForeignKey(Cate,on_delete=models.CASCADE)