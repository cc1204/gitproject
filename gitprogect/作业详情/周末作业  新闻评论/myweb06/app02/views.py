from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response

# Create your views here.
from app02.models import Cate,Fiction
import requests
from bs4 import BeautifulSoup

url='http://www.23wxc.com/'
headers={
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'
}
class CateApi(APIView):
    def get(self,request):
        """

        """
        #模拟网页登陆
        resp=requests.get(url=url,headers=headers)
        #获取一下编码格式
        resp.encoding=resp.apparent_encoding    #击沉或网页中的编码格式
        #使用bs4来解析网页内容
        html_obj=BeautifulSoup(resp.text,features='lxml')
        print(html_obj)
        #find()     查找一个内容，里面的查找条件是标签名+class或者id
        #find_all()     查找所有的内容，查找条件可是标签名+class,返回内容是列表
        li_list=html_obj.find('ui').find_all('li')[2: -4]
        print(li_list)
        li_obj=[]
        for li in li_list:
            name=li.find('a').text
            if Cate.objects.filter(cate_name=name):
            #条件成立，不加添加
                pass
            else:
                li_obj.append(
                    Cate(
                        cate_name=li.find('a').text,
                        link=url+li.find('a')['href']
                    )
                )
                print(li_obj)
        Cate.objects.bulk_create(li_obj)    #bulk_create()  批量添加数据库
        return Response({'msg':'获取完成','code':200})