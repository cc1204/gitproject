from django.urls import path
from .views import *

urlpatterns=[
    path('add_news_view/',AddNewsView.as_view()),    #添加新闻接口
    path('show_news_view/',ShowNewsView.as_view()),    #展示新闻接口
    path('login_view/',LoginView.as_view()),    #用户登陆接口
    path('add_comment_view/',AddCommentView.as_view()),    #评论接口
    path('show_content_view/<news_id>',ShowContentView.as_view()),    #展示新闻内容接口
    path('show_comment_view/<news_id>',ShowCommentView.as_view()),    #展示评论内容接口
    path('show_news_api/',ShowNewsApi.as_view()),    #展示浏览大于评论新闻接口
]