from django.db.models import F,Q
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import User,News,Comment
from app01.serializer import UserModelSerializer,NewsModelSerializer,CommentModelSerializer


class AddNewsView(APIView):
    def post(self,request):
        if News.objects.filter(title=request.data.get('title')):
            return Response({'msg':'该新闻已存在','code':400})
        news_data=request.data
        news_serializer=NewsModelSerializer(data=news_data)
        if news_serializer.is_valid():
            news_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(news_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})

class ShowNewsView(APIView):
    def get(self,request):
        news_list=News.objects.all()
        news_serializer=NewsModelSerializer(news_list,many=True)
        return Response(news_serializer.data)

class LoginView(APIView):
    def post(self,request):
        print(request.data)
        user_obj=User.objects.filter(username=request.data.get('username')).first()
        if user_obj:
            if user_obj.password==request.data.get('password'):
                return Response({'msg': '登陆成功', 'code': 200,'user_id':user_obj.pk})
            else:
                return Response({'msg': '用户名或密码不正确', 'code': 400})
        else:
            return Response({'msg': '用户名或密码不正确', 'code': 400})

class AddCommentView(APIView):
    def post(self,request):
        print(request.data)
        user=request.data.get('user')
        news=request.data.get('news')
        comment_obj=Comment.objects.filter(user=user,news=news)
        if comment_obj.count() <3:
            # 条件成立时，进行评论
            comment_serializer = CommentModelSerializer(data=request.data)
            if comment_serializer.is_valid():
                comment_serializer.save()
                news_obj=News.objects.get(pk=news)
                news_obj.total_comment+=1
                news_obj.save()
                return Response({'msg': '评论成功', 'code': 200})
            else:
                return Response({'msg': '评论失败', 'code': 400})
        else:
            return Response({'msg': '评论次数受限', 'code': 400})


class ShowContentView(APIView):
    def get(self,request,news_id):
        news_obj=News.objects.get(pk=news_id)
        news_obj.total_views+=1
        news_obj.save()
        news_serializer=NewsModelSerializer(news_obj)
        return Response(news_serializer.data)
class ShowCommentView(APIView):
    def get(self,request,news_id):
        news_obj=News.objects.get(pk=news_id)
        comment_list=Comment.objects.filter(news=news_obj)
        comment_serializer=CommentModelSerializer(comment_list,many=True)
        return Response(comment_serializer.data)

class ShowNewsApi(APIView):
    def get(self,request):
        news_list=News.objects.filter(total_views__gt=F('total_comment'))
        news_serializer=NewsModelSerializer(news_list,many=True)
        return Response(news_serializer.data)
