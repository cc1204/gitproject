from django.db import models

# Create your models here.

class User(models.Model):
    username=models.CharField(max_length=30,verbose_name='用户名')
    password=models.CharField(max_length=128,verbose_name='密码')
    class Meta:
        db_table='user'
    def __str__(self):
        return self.username

class News(models.Model):
    title=models.CharField(max_length=30,verbose_name='标题')
    content=models.CharField(max_length=300,verbose_name='内容')
    total_comment=models.IntegerField(default=0,verbose_name='总评论数')
    total_views=models.IntegerField(default=0,verbose_name='总浏览数')
    class Meta:
        db_table='news'
    def __str__(self):
        return self.content

class Comment(models.Model):
    content = models.CharField(max_length=300, verbose_name='评论内容')
    user=models.ForeignKey(User,on_delete=models.CASCADE,verbose_name='关联用户')
    news=models.ForeignKey(News,on_delete=models.CASCADE,verbose_name='关联新闻')
    comment_count=models.IntegerField(default=0,verbose_name='用户评论次数')
    class Meta:
        db_table='comment'
    def __str__(self):
        return self.content

