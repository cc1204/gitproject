from rest_framework import serializers
from app01.models import User,News,Comment

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields="__all__"

class NewsModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=News
        fields="__all__"

class CommentModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Comment
        fields="__all__"