import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddNews from '@/components/AddNews'
import ShowNews from '@/components/ShowNews'
import Login from '@/components/Login'
import AddComment from '@/components/AddComment'
import ShowContent from '@/components/ShowContent'
import ShowRedNews from '@/components/ShowRedNews'
import Spider from '@/components/Spider'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddNews',
      name: 'AddNews',
      component: AddNews
    },
    {
      path: '/ShowNews',
      name: 'ShowNews',
      component: ShowNews
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/AddComment/:news_id',
      name: 'AddComment',
      component: AddComment
    },
    {
      path: '/ShowContent/:news_id',
      name: 'ShowContent',
      component: ShowContent
    },
    {
      path: '/ShowRedNews',
      name: 'ShowRedNews',
      component: ShowRedNews
    },
    {
      path: '/Spider',
      name: 'Spider',
      component: Spider
    },
    
    
    

  ]
})
