import re

from django.core.paginator import Paginator
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import Cate,Goods,Order
from app01.serializer import CateModelSerializer,GoodsModelSerializer,OrderModelSerializer

class CateView(APIView):
    def post(self,request):
        print(request.data)
        cate_obj=Cate.objects.filter(name=request.data.get('name'))
        if cate_obj:
            return Response({'msg':'该分类已存在','code':400})
        cate_serializer=CateModelSerializer(data=request.data)
        if cate_serializer.is_valid():
            cate_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(cate_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        cate_list=Cate.objects.all()
        cate_serializer=CateModelSerializer(cate_list,many=True)
        return Response(cate_serializer.data)

class GoodsView(APIView):
    def post(self,request):
        print(request.data)
        goods_obj=Goods.objects.filter(goods_name=request.data.get('goods_name'))
        if goods_obj:
            return Response({'msg':'该商品已存在','code':400})
        price=request.data.get('price')
        if not re.match('\d{1,5}',price):
            return Response({'msg':'价格格式不规范','code':400})
        goods_serializer=GoodsModelSerializer(data=request.data)
        if goods_serializer.is_valid():
            goods_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(goods_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        pid=request.GET.get('pid')
        cate_obj=int(request.GET.get('cate'))
        if cate_obj:
            goods_list=Goods.objects.filter(cate=cate_obj)
        else:
            goods_list=Goods.objects.all()
        paginotor=Paginator(goods_list,3)
        try:
            paged = paginotor.page(pid)
        except:
            paged = paginotor.page(1)

        goods_serializer=GoodsModelSerializer(paged,many=True)
        return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':goods_serializer.data})


class ShowGoodsApi(APIView):
    def get(self,request):
        gid=request.GET.get('gid')
        goods_obj=Goods.objects.get(pk=gid)
        goods_serializer=GoodsModelSerializer(goods_obj)
        return Response(goods_serializer.data)


class OrderView(APIView):
    def post(self,request):
        print(request.data)
        order_serializer = OrderModelSerializer(data=request.data)
        if order_serializer.is_valid():
            order_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            print(order_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        order_list=Order.objects.all()
        order_serializer=OrderModelSerializer(order_list,many=True)
        return Response(order_serializer.data)

class UpdateStatusView(APIView):
    def get(self,request,cid):
        order_obj=Order.objects.filter(pk=cid).first()
        if order_obj.status==True:
            order_obj.status=False
            order_obj.save()
            return Response({'msg':'OK'})
        else:
            order_obj.status=True
            order_obj.save()
            return Response({'msg': 'OK'})






