# Generated by Django 2.1.8 on 2020-06-17 16:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
            ],
            options={
                'db_table': 'cate',
            },
        ),
        migrations.CreateModel(
            name='Goods',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('goods_name', models.CharField(max_length=32)),
                ('price', models.DecimalField(decimal_places=2, max_digits=20)),
                ('img', models.ImageField(upload_to='img')),
                ('source', models.IntegerField(default=0)),
                ('cate', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app01.Cate')),
            ],
            options={
                'db_table': 'goods',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_name', models.CharField(max_length=32, verbose_name='甜品名称')),
                ('num', models.IntegerField(verbose_name='预定数量')),
                ('username', models.CharField(max_length=30, verbose_name='收货姓名')),
                ('address', models.CharField(max_length=128, verbose_name='收货地址')),
                ('phone_number', models.CharField(max_length=11, verbose_name='联系电话')),
                ('content', models.CharField(max_length=300, verbose_name='内容')),
                ('status', models.BooleanField(default=False, verbose_name='是否支付')),
            ],
            options={
                'db_table': 'orders',
            },
        ),
    ]
