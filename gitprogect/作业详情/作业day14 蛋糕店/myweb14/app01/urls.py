from django.urls import path
from .views import *
urlpatterns=[
    path('cate_view/',CateView.as_view()),
    path('goods_view/',GoodsView.as_view()),
    path('show_goods_api/',ShowGoodsApi.as_view()),
    path('order_view/',OrderView.as_view()),
    path('update_status_view/<cid>',UpdateStatusView.as_view()),
]