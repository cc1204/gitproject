from rest_framework import serializers
from app01.models import Cate,Goods,Order

class CateModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Cate
        fields="__all__"

class GoodsModelSerializer(serializers.ModelSerializer):
    cate_name=serializers.CharField(source='cate.name',default=True)
    class Meta:
        model=Goods
        fields="__all__"

class OrderModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Order
        fields="__all__"