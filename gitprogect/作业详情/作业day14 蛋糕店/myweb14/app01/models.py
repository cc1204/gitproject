from django.db import models

# Create your models here.

class Cate(models.Model):
    name=models.CharField(max_length=32)
    class Meta:
        db_table='cate'
    def __str__(self):
        return self.name


#商品表
class Goods(models.Model):
    goods_name = models.CharField(max_length=32)
    price=models.DecimalField(max_digits=20,decimal_places=2)
    img=models.ImageField(upload_to='img')
    source=models.IntegerField(default=0)
    cate=models.ForeignKey(Cate,on_delete=models.CASCADE)
    class Meta:
        db_table = 'goods'

    def __str__(self):
        return self.goods_name

#订单表

class Order(models.Model):
    order_name = models.CharField(max_length=32,verbose_name='甜品名称')
    num=models.IntegerField(verbose_name='预定数量')
    username=models.CharField(max_length=30,verbose_name='收货姓名')
    address=models.CharField(max_length=128,verbose_name='收货地址')
    phone_number=models.CharField(max_length=11,verbose_name='联系电话')
    content=models.CharField(max_length=300,verbose_name='内容')
    status=models.BooleanField(default=False,verbose_name='是否支付')

    class Meta:
        db_table = 'orders'

    def __str__(self):
        return self.order_name