import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddCate from '@/components/AddCate'
import AddGoods from '@/components/AddGoods'
import Index from '@/components/Index'
import Order from '@/components/Order'
import ShowOrder from '@/components/ShowOrder'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddCate',
      name: 'AddCate',
      component: AddCate
    },
    {
      path: '/AddGoods',
      name: 'AddGoods',
      component: AddGoods
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },
    {
      path: '/Order/:gid',
      name: 'Order',
      component: Order
    },
    {
      path: '/ShowOrder',
      name: 'ShowOrder',
      component: ShowOrder
    },
  ]
})
