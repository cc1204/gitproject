import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddAuthor from '@/components/AddAuthor'
import Login from '@/components/Login'
import AddBooks from '@/components/AddBooks'
import Index from '@/components/Index'
import UpdateAuthor from '@/components/UpdateAuthor'
// import HelloWorld from '@/components/HelloWorld'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddAuthor',
      name: 'AddAuthor',
      component: AddAuthor
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/AddBooks',
      name: 'AddBooks',
      component: AddBooks,
      beforeEnter(to,from,next){
        let user_id=sessionStorage.getItem('user_id')
        if(user_id){
          next()
        }else{
          next('/Login')
        }
      }
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index,
      beforeEnter(to,from,next){
        let user_id=sessionStorage.getItem('user_id')
        if(user_id){
          next()
        }else{
          next('/Login')
        }
      }
    },
    {
      path: '/UpdateAuthor/:bid',
      name: 'UpdateAuthor',
      component: UpdateAuthor
    },
  ]
})
