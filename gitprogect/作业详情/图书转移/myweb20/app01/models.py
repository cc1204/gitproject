from django.db import models

# Create your models here.
#作者表
class Author(models.Model):
    username=models.CharField(max_length=30)
    password=models.CharField(max_length=128)
    class Meta:
        db_table='author'


# 图书表
class Book(models.Model):
    title=models.CharField(max_length=300)
    price=models.DecimalField(max_digits=8,decimal_places=2)
    author_id=models.ForeignKey(Author,on_delete=models.CASCADE)
    class Meta:
        db_table='book'

