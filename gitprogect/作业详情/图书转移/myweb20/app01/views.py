import re
from django.core.paginator import Paginator
from django.shortcuts import render

from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import Author,Book
from app01.serializer import AuthorModelSerializer,BookModelSerializer


#添加作者
class AuthorView(APIView):
    def post(self,request):
        # 获取网页提交数据
        print(request.data)
        username= request.data.get('username')
        password= request.data.get('password')
        user_obj=Author.objects.filter(username=username)
        if user_obj:
            return Response({'msg':'作者已存在','code':400})
        data={
            'username':username,
            'password':password
        }
        user_serializer=AuthorModelSerializer(data=data)
        if user_serializer.is_valid():
            # 校验通过
            user_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            # 返回错误信息
            print(user_serializer.errors)
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        author_list=Author.objects.all()
        author_serializer=AuthorModelSerializer(author_list,many=True)
        return Response(author_serializer.data)


#登录
class LoginView(APIView):
    def post(self,request):
        # 获取网页提交数据
        print(request.data)
        username= request.data.get('username')
        password= request.data.get('password')
        user_obj=Author.objects.filter(username=username).first()
        if user_obj:
            if password==user_obj.password:
                return Response({'msg': '登录成功', 'code': 200,'user_id':user_obj.pk})
            else:
                return Response({'msg': '用户名或密码错误', 'code': 400})
        else:
            return Response({'msg': '用户名或密码错误', 'code': 400})

#添加图书
class BookView(APIView):
    def post(self,request):
        # 获取网页提交数据
        print(request.data)
        book_serializer=BookModelSerializer(data=request.data)
        if book_serializer.is_valid():
            # 校验通过
            book_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        author_id=request.GET.get('author_id')
        pid=request.GET.get('pid')
        author_obj=Author.objects.get(pk=author_id)
        book_list=Book.objects.filter(author_id=author_obj)
        paginotor=Paginator(book_list,3)
        paged=paginotor.page(pid)
        book_serializer=BookModelSerializer(paged,many=True)
        return Response({
            'page_sum':paginotor.num_pages,
            'page_list':[i for i in paginotor.page_range],
            'data':book_serializer.data
        })

#获取作者
class ShowAuthor(APIView):
    def get(self,request):
        author_list=Author.objects.all()
        author_serializer=AuthorModelSerializer(author_list,many=True)
        return Response(author_serializer.data)

class Update(APIView):
    def post(self,request):
        # 获取网页提交数据
        print(request.data)
        bid=request.data.get('bid')
        author=request.data.get('author')
        book_obj=Book.objects.get(pk=bid)
        author_obj=Author.objects.get(pk=author)
        book_obj.author_id=author_obj
        book_obj.save()
        return Response({'msg':'修改成功','code':200})
