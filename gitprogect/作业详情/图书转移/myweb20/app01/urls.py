from django.urls import path
from .views import *

urlpatterns=[
    path('author_view/',AuthorView.as_view()),
    path('login_view/',LoginView.as_view()),
    path('book_view/',BookView.as_view()),
    path('show_author/',ShowAuthor.as_view()),
    path('update/',Update.as_view()),
]