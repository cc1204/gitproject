from rest_framework import serializers
from app01.models import Author,Book

class AuthorModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Author
        fields="__all__"


class BookModelSerializer(serializers.ModelSerializer):
    username=serializers.CharField(source='author_id.username',default=True)
    class Meta:
        model=Book
        fields="__all__"

