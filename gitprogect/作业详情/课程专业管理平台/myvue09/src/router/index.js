import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login'
import AddProfessional from '@/components/AddProfessional'
import AddCourse from '@/components/AddCourse'
import ShowCoursePage from '@/components/ShowCoursePage'
import ShowProfessional from '@/components/ShowProfessional'
import UpdateCourse from '@/components/UpdateCourse'
import UpdatePro from '@/components/UpdatePro'
import Index from '@/components/Index'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/AddProfessional',
      name: 'AddProfessional',
      component: AddProfessional
    },
    {
      path: '/AddCourse',
      name: 'AddCourse',
      component: AddCourse
    },
    {
      path: '/ShowCoursePage',
      name: 'ShowCoursePage',
      component: ShowCoursePage
    },
    {
      path: '/ShowProfessional',
      name: 'ShowProfessional',
      component: ShowProfessional
    },
    {
      path: '/UpdateCourse/:course_id',
      name: 'UpdateCourse',
      component: UpdateCourse
    },
    {
      path: '/UpdatePro/:pro_id',
      name: 'UpdatePro',
      component: UpdatePro
    },
    {
      path: '/Index',
      name: 'Index',
      component: Index
    },

  ]
})
