from django.db import models

# Create your models here.

#用户表
class User(models.Model):
    username=models.CharField(max_length=30)
    password=models.CharField(max_length=128)
    status=models.BooleanField(default=True)
    auth=models.IntegerField(default=0)
    addtime=models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table='user'

#专业表
class Professional(models.Model):
    name=models.CharField(max_length=30)
    status=models.BooleanField(default=True)
    addtime=models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table='professional'

#课程表
class Course(models.Model):
    name=models.CharField(max_length=30)
    status=models.BooleanField(default=True)
    addtime=models.DateTimeField(auto_now_add=True)
    professional=models.ForeignKey(Professional,on_delete=models.CASCADE,null=True)
    class Meta:
        db_table='course'


#日志表
class Log(models.Model):
    username=models.CharField(max_length=30)
    addtime=models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table='log'