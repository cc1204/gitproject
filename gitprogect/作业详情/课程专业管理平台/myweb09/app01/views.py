from django.contrib.auth.hashers import make_password, check_password
from django.core.paginator import Paginator
from django.db.models import Q
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.
from app01.models import User,Professional,Course,Log
from app01.serializer import UserModelSerializer,ProfessionalModelSerializer,CourseModelSerializer,LogModelSerializer

class RegisterView(APIView):
    def post(self,request):
        #获取网页提交数据
        username=request.data.get('username')
        password=request.data.get('password')
        #去重
        if User.objects.filter(username=username):
            return Response({'msg':'该用户已存在','code':400})
        #存储数据
        else:
            data={
                'username':username,
                'password':make_password(password)
            }
            user_serializer=UserModelSerializer(data=data)
            if user_serializer.is_valid():
                user_serializer.save()
                return Response({'msg': '注册成功', 'code': 200})
            else:
                print(user_serializer.errors)
                return Response({'msg': '注册失败', 'code': 400})

class LoginView(APIView):
    def post(self,request):
        username = request.data.get('username')
        password = request.data.get('password')
        user_obj=User.objects.filter(username=username).first()
        if user_obj:
            if check_password(password,user_obj.password):
                self.addlog(username)
                return Response({'msg': '登录成功', 'code': 200,'auth':user_obj.auth})
            else:
                return Response({'msg': '用户名或密码错误', 'code': 400})
        else:
            return Response({'msg': '用户名或密码错误', 'code': 400})
    def addlog(self,username):
        Log.objects.create(username=username)

class ProfessionalView(APIView):
    def post(self,request):
        print(request.data)
        if Professional.objects.filter(name=request.data.get('name')):
            return Response({'msg': '该专业已存在', 'code': 400})
        professional_serializer=ProfessionalModelSerializer(data=request.data)
        if professional_serializer.is_valid():
            professional_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            return Response({'msg': '添加失败', 'code': 400})
    def get(self,request):
        professional_list=Professional.objects.all()
        professional_serializer=ProfessionalModelSerializer(professional_list,many=True)
        return Response(professional_serializer.data)

class AddCourseView(APIView):
    def post(self,request):
        print(request.data)
        if Course.objects.filter(name=request.data.get('name')):
            return Response({'msg': '该课程已存在', 'code': 400})
        course_serializer=CourseModelSerializer(data=request.data)
        if course_serializer.is_valid():
            course_serializer.save()
            return Response({'msg': '添加成功', 'code': 200})
        else:
            return Response({'msg': '添加失败', 'code': 400})

class ShowCoursePageView(APIView):
    def get(self,request,pid):
        search_name=request.GET.get('search_name')
        course_list=Course.objects.filter(Q(professional__name__contains=search_name)|Q(name__contains=search_name)).order_by('-addtime').all()
        paginotor=Paginator(course_list,3)
        paged=paginotor.page(pid)
        course_serializer=CourseModelSerializer(paged,many=True)
        return Response({'page_sum':paginotor.num_pages,'page_list':[i for i in paginotor.page_range],'data':course_serializer.data})

class ShowProfessional(APIView):
    def get(self,request,pid):
        professional_list=Professional.objects.all().order_by('-addtime')
        paginotor=Paginator(professional_list,3)
        paged=paginotor.page(pid)
        professional_serializer=ProfessionalModelSerializer(paged,many=True)
        return Response({'page_sum': paginotor.num_pages, 'page_list': [i for i in paginotor.page_range],
                         'data': professional_serializer.data})

class DelCourseView(APIView):
    def get(self,request,cid):
        course_obj=Course.objects.get(pk=cid)
        course_obj.delete()
        return Response({'msg':'删除成功','code':200})

class ShowCourseApi(APIView):
    def get(self,request,course_id):
        course_list=Course.objects.get(pk=course_id)
        course_serializer=CourseModelSerializer(course_list)
        return Response(course_serializer.data)

class UpdateCourseView(APIView):
    def post(self,request):
        name=request.data.get('name')
        status=request.data.get('status')
        course_id=request.data.get('course_id')
        course_obj=Course.objects.get(pk=course_id)
        course_obj.name=name
        course_obj.status=status
        course_obj.save()
        return Response({'msg': '修改成功', 'code': 200})

class DelProView(APIView):
    def get(self,request,cid):
        pro_obj=Professional.objects.get(pk=cid)
        pro_obj.delete()
        return Response({'msg':'删除成功','code':200})

class ShowProApi(APIView):
    def get(self,request,pro_id):
        pro_list=Professional.objects.get(pk=pro_id)
        pro_serializer=ProfessionalModelSerializer(pro_list)
        return Response(pro_serializer.data)

class UpdateProView(APIView):
    def post(self,request):
        name=request.data.get('name')
        status=request.data.get('status')
        pro_id=request.data.get('pro_id')
        pro_obj=Professional.objects.get(pk=pro_id)
        pro_obj.name=name
        pro_obj.status=status
        pro_obj.save()
        print(pro_obj.status)
        return Response({'msg': '修改成功', 'code': 200})