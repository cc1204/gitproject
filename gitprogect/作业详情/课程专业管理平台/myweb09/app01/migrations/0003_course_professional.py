# Generated by Django 2.1.8 on 2020-06-11 00:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0002_auto_20200611_0016'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='professional',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='app01.Professional'),
        ),
    ]
