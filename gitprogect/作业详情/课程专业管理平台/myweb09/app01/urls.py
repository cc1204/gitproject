from django.urls import path
from .views import *

urlpatterns=[
    path('register_view/',RegisterView.as_view()),
    path('login_view/',LoginView.as_view()),
    path('professional_view/',ProfessionalView.as_view()),
    path('add_course_view/',AddCourseView.as_view()),
    path('show_course_page_view/<pid>',ShowCoursePageView.as_view()),
    path('show_professional_view/<pid>',ShowProfessional.as_view()),
    path('del_course_view/<cid>',DelCourseView.as_view()),
    path('show_course_api/<course_id>',ShowCourseApi.as_view()),
    path('update_course_view/',UpdateCourseView.as_view()),
    path('del_pro_view/<cid>',DelProView.as_view()),
    path('show_pro_api/<pro_id>',ShowProApi.as_view()),
    path('update_pro_view/',UpdateProView.as_view()),
]