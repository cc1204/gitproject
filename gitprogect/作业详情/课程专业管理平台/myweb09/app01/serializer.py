from rest_framework import serializers
from app01.models import User,Professional,Course,Log

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields="__all__"

class ProfessionalModelSerializer(serializers.ModelSerializer):
    course_name=serializers.SerializerMethodField()
    class Meta:
        model=Professional
        fields="__all__"
    def get_course_name(self,obj):
        data=[i.name for i in obj.course_set.all()]
        return '、'.join(data)




class CourseModelSerializer(serializers.ModelSerializer):
    professional_name=serializers.CharField(source="professional.name")
    class Meta:
        model=Course
        fields="__all__"

class LogModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Log
        fields="__all__"