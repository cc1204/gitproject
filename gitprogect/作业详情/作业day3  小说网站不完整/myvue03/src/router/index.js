import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import AddGrade from '@/components/AddGrade'
import UpdateGrade from '@/components/UpdateGrade'
import AddUser from '@/components/AddUser'
import ShowUser from '@/components/ShowUser'
import UpdateUser from '@/components/UpdateUser'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/AddGrade',
      name: 'AddGrade',
      component: AddGrade
    },
    {
      path: '/UpdateGrade/:grade_id',
      name: 'UpdateGrade',
      component: UpdateGrade
    },
    {
      path: '/AddUser',
      name: 'AddUser',
      component: AddUser
    },
    {
      path: '/ShowUser',
      name: 'ShowUser',
      component: ShowUser
    },
    {
      path: '/UpdateUser/:user_id',
      name: 'UpdateUser',
      component: UpdateUser
    },
  ]
})
