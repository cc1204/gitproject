from rest_framework import serializers
from app01.models import Grade,User,Information,Author,Cate,Story,Section

class GradeModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Grade
        fields="__all__"

class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields="__all__"

class InformationModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Information
        fields="__all__"

class AuthorModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Author
        fields="__all__"

class CateModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Cate
        fields="__all__"

class StoryModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Story
        fields="__all__"

class SectionModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=Section
        fields="__all__"


