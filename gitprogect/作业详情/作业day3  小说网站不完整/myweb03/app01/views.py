from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import  Response
import re
from django.conf import settings
# Create your views here.
from app01.models import Grade,User,Information,Author,Cate,Story,Section
from app01.serializer import GradeModelSerializer,UserModelSerializer,AuthorModelSerializer,InformationModelSerializer,CateModelSerializer,StoryModelSerializer,SectionModelSerializer
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,SignatureExpired

serializer=Serializer(settings.SECRET_KEY,60)

#等级表增删改查、

class GradeView(APIView):
    def post(self,request):
        print(request.data)
        grade_data=request.data
        grade_serializer=GradeModelSerializer(data=grade_data)
        if grade_serializer.is_valid():
            grade_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            print(grade_serializer.errors)
            return Response({'msg': '添加失败', 'code': 200})
    def get(self,request):
        grade_list=Grade.objects.all()
        grade_serializer=GradeModelSerializer(grade_list,many=True)
        return Response(grade_serializer.data)

class DeleteGradeView(APIView):
    def get(self,request,cid):
        grade_obj = Grade.objects.get(pk=cid)
        grade_obj.delete()
        return Response({'msg': '删除成功', 'code': 200})

class ShowGradeView(APIView):
    def get(self,request,grade_id):
        grade_obj=Grade.objects.get(pk=grade_id)
        grade_serializer=GradeModelSerializer(grade_obj)
        return Response(grade_serializer.data)

class UpdateGradeView(APIView):
    def post(self,request):
        grade_name=request.data.get('grade_name')
        grade_id=request.data.get('grade_id')
        grade_obj=Grade.objects.get(pk=grade_id)
        grade_obj.grade_name=grade_name
        grade_obj.save()
        return Response({'msg':'修改成功','code':200})

#用户表增删改查

class AddUserView(APIView):
    def post(self,request):
        print(request.data)
        username=request.data.get('username')
        password1=request.data.get('password1')
        password2=request.data.get('password2')
        user_email=request.data.get('user_email')
        if not re.match(r'[0-9a-zA-Z_]{0,19}@(.*?).com',user_email):
            return Response({'msg': '邮箱不符合规则', 'code': 400})
        user_obj=User.objects.filter(username=username)
        if user_obj:
            return Response({'msg':'该用户已存在','code':400})
        else:
            if password1==password2:
                data={
                    'username':username,
                    'password':make_password(password1),
                    'user_email':user_email,
                    'user_status':request.data.get('user_status')
                }
                user_serializer=UserModelSerializer(data=data)
                if user_serializer.is_valid():
                    user_serializer.save()
                    user_info={'user_id':request.data.get('id')}
                    token=serializer.dumps(user_info).decode()
                    subject='微信读书会员注册'
                    message='欢迎注册微信读书商场会员'
                    from_email=settings.EMAIL_FROM
                    recipient_list=[user_email]
                    html_message='<h3>欢迎注册微信读书商场会员,请点击以下链接进行激活</br><a href="http://127.0.0.1:8000/app01/add_user_view/?token={}">激活会员请点击这里</a></h3>'.format(token)
                    send_mail(subject=subject,message=message,from_email=from_email,recipient_list=recipient_list,html_message=html_message)
                    return Response({'msg': '注册成功', 'code': 200})
                else:
                    return Response({'msg': '注册失败', 'code': 400})
            else:
                return Response({'msg': '两次密码输入不一致', 'code': 400})
    def get(self,request):
        print(request.GET.get('token'))
        token=request.GET.get('token')
        try:
            user_info=serializer.loads(token)
            print(user_info)
            return Response({'msg':'OK','code':200,'token':user_info})
        except SignatureExpired:
            return Response({'msg':'激活失败','code':400})

class ShowUserView(APIView):
    def get(self,request):
        user_list=User.objects.all()
        user_serializer=UserModelSerializer(user_list,many=True)
        return Response(user_serializer.data)

class DeleteUserView(APIView):
    def get(self,request,cid):
        user_obj=User.objects.get(pk=cid)
        user_obj.delete()
        return Response({'msg':'删除成功','code':200})

class UpdateUserView(APIView):
    def post(self,request):
        username=request.data.get('username')
        user_status=request.data.get('user_status')
        user_email=request.data.get('user_email')
        user_id=request.data.get('user_id')
        user_obj=User.objects.get(pk=user_id)
        user_obj.username=username
        user_obj.user_status=user_status
        user_obj.user_email=user_email
        user_obj.save()
        return Response({'msg':'修改成功','code':200})

class ShowUserDetailView(APIView):
    def get(self,request,user_id):
        user_list=User.objects.get(pk=user_id)
        user_serializer=UserModelSerializer(user_list)
        return Response(user_serializer.data)