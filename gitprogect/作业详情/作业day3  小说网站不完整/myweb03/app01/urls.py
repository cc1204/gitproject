from django.urls import path
from .views import GradeView,DeleteGradeView,ShowGradeView,UpdateGradeView,AddUserView,ShowUserView,DeleteUserView,UpdateUserView,ShowUserDetailView

urlpatterns=[
    path('grade_view/',GradeView.as_view()),
    path('delete_grade_view/<cid>',DeleteGradeView.as_view()),
    path('show_grade_view/<grade_id>',ShowGradeView.as_view()),
    path('update_grade_view/',UpdateGradeView.as_view()),
    path('add_user_view/',AddUserView.as_view()),
    path('show_user_view/',ShowUserView.as_view()),
    path('delete_user_view/<cid>',DeleteUserView.as_view()),
    path('update_user_view/',UpdateUserView.as_view()),
    path('show_user_detail_view/<user_id>',ShowUserDetailView.as_view()),
]