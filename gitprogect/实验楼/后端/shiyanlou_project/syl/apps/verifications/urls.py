# -*- coding: utf-8 -*-
from django.urls import path
from . import views
urlpatterns = [
    path('image_codes/',views.ImageCodeView.as_view()),
    path('sms_codes/',views.SmsCodeView.as_view()),
]