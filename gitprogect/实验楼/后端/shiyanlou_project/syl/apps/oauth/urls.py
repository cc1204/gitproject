# -*- coding: utf-8 -*-
from django.urls import path
from . import views
urlpatterns = [
    path('weibo/',views.WeiboUrl.as_view())  ,      # /oauth/weibo/ 返回微博登录地址
    path('weibo/callback/', views.OauthWeiboCallback.as_view())  ,      # /oauth/weibo/callback/
    path('weibo/binduser/',views.OauthWeiboBindUser.as_view()),      # /oauth/weibo/callback/
    path('qntoken/',views.QNYTokenView.as_view()),
]