from django.shortcuts import render
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from urllib.parse import urlencode
from user.models import User
# Create your views here.

# 生成前端跳转到微博扫码页面的url
class WeiboUrl(APIView):
    '''
    生成微博的登陆页面路由地址
    https://api.weibo.com/oauth2/authorize?
    # 微博oauth认证地址 client_id=4152203033&
    # 注册开发者id response_type=code&
    redirect_uri=http://127.0.0.1:8888/oauth/callback/ # 获取code后将code回 调给后端地址
    '''
    # 自定义权限类
    permission_classes = (AllowAny,)

    def post(self, request):
        url = 'https://api.weibo.com/oauth2/authorize?'       # 微博授权的 url地址 data = { 'client_id': '3516473472', # WEIBO_APP_KEY,'response_type': 'code', 'redirect_uri': 'http://127.0.0.1:8888/oauth/callback/', # VUE的回 调，微博后台授权的回调地址 }weibo_url = url + urlencode(data) # https://api.weibo.com/oauth2/authorize? client_id=4152203033&response_type=code&redirect_uri=http://127.0.0.1:8000/api/we ibo_back/# return Response({'weibo_url': weibo_url}) return Response({'code': '0', 'msg': '成功', 'data': {'url': weibo_url}})
        data = {
            'client_id': '2112309376',
        # WEIBO_APP_KEY,
        # 'response_type': 'code',
        'redirect_uri': 'http://127.0.0.1:8888/oauth/callback/',
        # VUE的回 调，微博后台授权的回调地址
        }
        weibo_url = url + urlencode(data)
        # https://api.weibo.com/oauth2/authorize? client_id=4152203033&response_type=code&redirect_uri=http://127.0.0.1:8000/api/we ibo_back/
        # return Response({'weibo_url': weibo_url})
        return Response({'code': '0', 'msg': '成功', 'data': {'url': weibo_url}})





import requests
from .models import OauthUser
from rest_framework_jwt.serializers import jwt_payload_handler,jwt_encode_handler
from user.utils import jwt_response_payload_handler

# 通过vue前端传入的code，微博身份验证
class OauthWeiboCallback(APIView):
    # 自定义权限类
    permission_classes = (AllowAny,)

    def post(self, request):
    # 接收vue端传过来的code（微博的用户code）
    # 1.使用微博用户code+微博开发者账号信息换取微博的认证access_token
        code = request.data.get('code')
        data = {
            'client_id': '2112309376',
            'client_secret': 'cef7895f3fd72e62e7d8e0857299cdda',
            'grant_type': 'authorization_code',
            'code': code,
            'redirect_uri': 'http://127.0.0.1:8888/oauth/callback/',
        }
        url = 'https://api.weibo.com/oauth2/access_token'
        data = requests.post(url=url, data=data).json() # 拿取请求的返回结果
        access_token = data.get('uid') # 获取到的微博token
        weibo_uid = data.get('access_token') # 获取到少码用户的id # 2. 根据uid 查询绑定情况
        try:
            oauth_user = OauthUser.objects.get(uid=weibo_uid, oauth_type='1')
        except Exception as e:
            oauth_user = None
        # 返回动作, 登录成功/需要绑定用户 type 0 登录成功, 1, 授权成功, 需要绑定
        if oauth_user:
            # 4. 如果绑定了, 返回token, 登录成功
             user = oauth_user.user

             payload = jwt_payload_handler(user)
             token = jwt_encode_handler(payload)
             # jwt_response_payload_handler为user模块定义的jwt返回的信息
             data = jwt_response_payload_handler(token, user)

             data['type'] = '0'  # 指定为登录成功
             return Response({'code': 0, 'msg': '登录成功', 'data': data})
        else:
            # 5. 如果没绑定, 返回标志, 让前端跳转到绑定页面
             return Response({'code': 0, 'msg': '授权成功', 'data': {'type': '1', 'uid': weibo_uid}})


class OauthWeiboBindUser(APIView):

    permission_classes = (AllowAny,)

    def post(self, request):
        # 绑定用户, 1. 已注册用户, 2. 未注册用户
        # 1.1 获取用户名, 密码, weibo_uid
        username = request.data.get('username')
        password = request.data.get('password')
        weibo_uid = request.data.get('weibo_uid')
        if not all([username, password, weibo_uid]):
            return Response({'code': 999, 'msg': '参数不全'})
        # 0.判断是否存在此用户
        try:
            user = User.objects.get(username=username)
        except Exception as e:
            user = None
        # 1. 已注册用户
        if user:
            # 1.2 , 如果存在就验证 密码, 验证通过,就绑定, 返回token,登录成功
            if user.check_password(password):
                ou = OauthUser(uid=weibo_uid, user=user, oauth_type='1')
                ou.save()
                payload = jwt_payload_handler(user) # 通过user对象获取到jwt的 payload信息
                token = jwt_encode_handler(payload) # 生成token
                data = jwt_response_payload_handler(token, user)
                data['type'] = '0' # 指定为登录成功
                return Response({'code': 0, 'msg': '登录成功', 'data': data})
            else:
                return Response({'code': 999, 'msg': '密码错误'})
        else:
            # 2. 未注册用户
            # 2.1 生成新用户, 设置用户名密码, 保存, 然后绑定, 返回token, 登录成功
            user = User(username=username)
            user.set_password(password)
            user.save()
            ou = OauthUser(uid=weibo_uid, user=user, oauth_type='1')
            ou.save()
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            data = jwt_response_payload_handler(token, user)
            data['type'] = '0' # 指定为登录成功
            return Response({'code': 0, 'msg': '登录成功', 'data': data})


class QNYTokenView(APIView):
    def get(self,request):
        from qiniu import Auth, put_file, etag
        import qiniu.config
        # 需要填写你的 Access Key 和 Secret Key
        access_key = 'lwcypI0q1sVvsDCkMrwRaZRkx09oW-tIQchOzItu'
        secret_key = 'Z3Dmst18gD7TXyldRt4NZVbbez9CF0oe8WgShTpX'
        # 构建鉴权对象
        q = Auth(access_key, secret_key)
        # 要上传的空间
        bucket_name = 'shiyanlou-videos'
        # 上传后保存的文件名
        key = None
        policy = {

        }
        # 生成上传 Token，可以指定过期时间等
        token = q.upload_token(bucket_name, key, 300000, policy)
        # 要上传文件的本地路径
        return Response({'uptoken':token})
