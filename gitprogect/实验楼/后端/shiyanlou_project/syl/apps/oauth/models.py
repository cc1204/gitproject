from django.db import models

# Create your models here.
class OauthUser(models.Model):
    OAUTHTYPE = (
        ('1', 'weibo'),
        ('2', 'weixin'),
    )
    uid = models.CharField('三方用户id', max_length=64)
    # 三方用户id
    user = models.ForeignKey('user.User', on_delete=models.CASCADE)
    # 本地用户外键，关联User表
    oauth_type = models.CharField('认证类型', max_length=10, choices=OAUTHTYPE)
    # 1,2 ...