# -*- coding: utf-8 -*-
from rest_framework import serializers
from course.models import CourseType,CourseTag,Course,Chapters,Sections,Base,JieDuan,Path
from goods.serializer import GoodsSerializer


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = '__all__'



class CourseTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseTag
        fields = '__all__'


class CourseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseType
        fields = '__all__'


class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sections
        fields = '__all__'

class ChaptersSerializer(serializers.ModelSerializer):
    sections = SectionSerializer(many=True)

    class Meta:
        model = Chapters
        fields = '__all__'

class CourseDeepSerializer(CourseSerializer):
    goods_set = GoodsSerializer(many=True)
    chapters = ChaptersSerializer(many=True)


class JieDuanSerializer(serializers.ModelSerializer):
    course = CourseSerializer(many=True)

    class Meta:
        model = JieDuan
        fields = '__all__'

class PathDeepSerializer(serializers.ModelSerializer):
    jieduan = JieDuanSerializer(many=True)

    class Meta:
        model = Path
        exclude = ['user']

class PathSerializer(serializers.ModelSerializer):
    class Meta:
        model = Path
        fields = ('id','title','img','desc','course_total')

