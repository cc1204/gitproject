# -*- coding: utf-8 -*-
from django.urls import path,include
from course import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'course',views.CourseViewSet),
router.register(r'tag',views.CourseTagViewSet),
router.register(r'type',views.CourseTypeViewSet),
router.register(r'sections',views.SectionsViewSet),
router.register(r'chapters',views.ChaptersViewSet),
router.register(r'path',views.PathViewSet),

urlpatterns = [
    path('add_sec/',views.AddSecView.as_view())

]

urlpatterns += router.urls
