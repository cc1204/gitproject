from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
# Create your views here.
from course.models import *
from course.serializer import *
from rest_framework.views import APIView
from rest_framework.response import Response


# 分页(局部) :自定义分页器,局部
class PageNum(PageNumberPagination):
    page_size_query_param = 'page_size'


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseDeepSerializer

    pagination_class = PageNum


class CourseTagViewSet(viewsets.ModelViewSet):
    queryset = CourseTag.objects.all()
    serializer_class = CourseTagSerializer

    pagination_class = PageNum

class CourseTypeViewSet(viewsets.ModelViewSet):
    queryset = CourseType.objects.all()
    serializer_class = CourseTypeSerializer

    pagination_class = PageNum

class SectionsViewSet(viewsets.ModelViewSet):
    queryset = Sections.objects.all()
    serializer_class = SectionSerializer

    pagination_class = PageNum

class ChaptersViewSet(viewsets.ModelViewSet):
    queryset = Chapters.objects.all()
    serializer_class = ChaptersSerializer

    pagination_class = PageNum

class PathViewSet(viewsets.ModelViewSet):
    queryset = Path.objects.all()

    def get_serializer_class(self):
        if self.action =='list':
            return PathSerializer
        else:
            return PathDeepSerializer

    pagination_class = PageNum

class AddSecView(APIView):
    def post(self,request):
        print(request.data)
        sec_serializer = SectionSerializer(data=request.data)
        if sec_serializer.is_valid():
            sec_serializer.save()
            return Response({'msg':'添加成功','code':200})
        else:
            return Response({'msg':'添加失败','code':400})







