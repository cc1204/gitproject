from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from goods.models import Goods,Orders
from goods.utils import get_order_id, get_pay_url, alipay
# Create your views here.


from decimal import Decimal

class PayUrlView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        # 1. 获取课程id,获取购买途径(普通购买, 促销购买) 获取用户
        goods_id = request.data.get('goods_id')
        goods = Goods.objects.get(id=goods_id)
        user = request.user
        # 2. 下定单
        order_id = get_order_id()
        if user.vip.vip_type == '1': # 普通会员
            goods_price = goods.price * Decimal('0.80').quantize(Decimal('0.00'))
        elif user.vip.vip_type == '2': # 高级会员
            goods_price = goods.price * Decimal('0.60').quantize(Decimal('0.00'))
        else: # 普通用户
            goods_price = goods.price
        goods_price = Decimal(goods_price).quantize(Decimal('0.00'))
        order = Orders(user=user, goods=goods, order_id=order_id, pay_method=1, status=1, total_amount=goods_price)
        order.save()
        print(order.total_amount,type(order.total_amount))
        # 3. 根据订单 生成支付链接
        subject = "实验楼订单:%s, 价格:%s" % (order.order_id, order.total_amount)
        pay_url = get_pay_url(order.order_id, order.total_amount, subject)

        # 4. 返回链接
        return Response({"code": 0, "msg": "下单成功", "data": {"pay_url": pay_url}})