# -*- coding: utf-8 -*-
from alipay import AliPay

# 沙箱环境中 app 私钥
app_private_key_string = open('app_private_key.pem').read()
# 支付宝公钥
alipay_public_key_string = open( 'alipay_public_key.pem').read()


def get_alipay_url():
    # 实例化一个alipay对象
    alipay = AliPay(
        appid="2016102600762809", # 沙箱appid
        app_notify_url=None, # 默认回调url
        app_private_key_string=app_private_key_string,
        # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
        alipay_public_key_string=alipay_public_key_string,
        sign_type="RSA2", # RSA 或者 RSA2
        debug=True, # 默认False,我们是沙箱，所以改成True(让访问沙箱环境支付宝地址)
    )
# 调用支付接口，生成支付链接
# 电脑网站支付，需要跳转到https://openapi.alipay.com/gateway.do? + order_string
    order_string = alipay.api_alipay_trade_page_pay(
        out_trade_no="201612226", # 订单id，应该从前端获取
        total_amount=str(0.01), # 订单总金额
        subject="测试阿里云付款",  # 付款标题信息
        return_url=None, # 付款成功回调地址(可以为空)
        notify_url=None # 付款成功后异步通知地址（可以为空）
    )
    pay_url = "https://openapi.alipaydev.com/gateway.do?" + order_string
    print(pay_url) # 将这个url复制到浏览器，就会打开支付宝支付页面
def query_pay():
    alipay = AliPay(
        appid="2016102600762809", # 沙箱appid
        app_notify_url=None, # 默认回调url
        app_private_key_string=app_private_key_string,
        # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥
        alipay_public_key_string=alipay_public_key_string,
        sign_type="RSA", # RSA 或者 RSA2
        debug=True, # 默认False,我们是沙箱，所以改成True(让访问沙箱环境支付宝地址)
    )
    trade_query = alipay.api_alipay_trade_query(
        out_trade_no=20161112, # 上面生成支付码页面时传入的商品订单号
        trade_no=None
    )
    print(trade_query)


if __name__ == '__main__':
    get_alipay_url()
    query_pay()