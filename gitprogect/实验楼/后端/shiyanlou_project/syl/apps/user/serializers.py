# -*- coding: utf-8 -*-
from rest_framework import serializers
from user.models import User



def address_validate(data):
    # 独立校验器
    # raise serializers.ValidationError('请填写实际地址')  # 有错就抛出异常
    # 没错就返回数据
    return data

    # 没错就返回数据



class UserSerializer(serializers.ModelSerializer):
    # 1.独立校验器，重新设定字段，替换掉模型中的设定，重新设定地址的长度为5
    address = serializers.CharField(max_length=255,min_length=5,validators=[address_validate])

    # 2.单一字段验证，验证地址,validate_字段名
    def validate_address(self,data):
        if data == '测试':
            raise serializers.ValidationError('请填写实际地址')  # 有错就抛出异常
        return data  # 没错就返回结果

    def validate_phone(self,data):
        # 不符合手机号格式
        # raise serializers.ValidationError('手机号格式不正确')
        model = self.root.Meta.model
        num = model.objects.filter(phone=data).count()
        if num > 0:
            raise serializers.ValidationError('手机号已存在')
        return data
    # 3.所有属性验证器
    def validate(self,attrs):
        # attrs:{"username":"zhangsan","phone":"15303478492",....}
        # 所有属性验证器
        # self.context 中有request和view上下文
        # self.context['view'].action 可以取到动作
        # attrs 是需要序列化的数据
        # raise serializers.ValidationError('xxx错误')  #有问题报错
        return attrs   # 没问题返回数据

    #定义序列化器显示内容
    class Meta:
        model = User        # 指定表
        # fields = （‘id’,） # 临时添加字段也需要写在这里
        fields = '__all__'  # 所有字段
        # exclude = ['id']  # 排除id字段
        read_only_fields = ('',)  # 指定字段为 read_only(只读)

        # 扩展address:  extra_kwargs = {}  # 局部替换某些字段的设定,或者新增设定
        extra_kwargs = {
            "address":{
                "min_length":5,  # 给地址增加最小长度限制
                "default":'默认测试地址',  # 增加默认值
            }
        }

    # class User(AbstractUser):
    # phone = models.CharField('手机号',max_length=20)
    # img = models.ImageField(upload_to='user',null=True)
    # nick_name = models.CharField('昵称',max_length=20)
    # address = models.CharField('地址',max_length=255)


class UserInfoSerializer(serializers.Serializer):
    id = serializers.CharField(read_only=True)  # 普通字段，设置id为只读字段，不能修改
    username = serializers.CharField(min_length=3, max_length=20, error_messages={'required': '该字段必填'})  # 显示普通字段
    img = serializers.ImageField(required=False)
    nick_name = serializers.CharField(max_length=20)
    address = serializers.CharField(max_length=255)
    xxx = serializers.SerializerMethodField(read_only=True)  # 自定义显示（显示多对多）

    class Meta:
        model = User
    # 自定义显示 多对多 字段


    def get_xxx(self, row):
        '''row: 传过来的正是 User表的对象'''
        users = row.username  # 获取用户名
        return users




    # 定义创建语法：ser.save()执行，就会立刻调用create方法用来创建数据
    def create(self, validated_data):
        '''validated_data: 表单或者vue请求携带的json：
    {"username":"zhangsan","password":"123456"}'''
        # https://www.cnblogs.com/xiaonq/p/7978409.html
        return User.objects.create(**validated_data)

    # 定义更新方法
    def update(self, instance, validated_data):
        '''
        instance : 查询的对象
        validated_data : postman提交的json数据
    {"username":"zhangsan","password":"123456"}
        '''
        if validated_data.get('username'):
            instance.username = validated_data['username']
        instance.save()
        return instance



    # 定义单一字段验证的方法
    def validate_name(self, value):
        if value == 'root':
            raise serializers.ValidationError('不能创建root管理员账号')
        return value


    # 定义多字段验证方法
    def validate(self, attrs):
        print(attrs)
        if attrs.get("username") == 'admin':
            raise serializers.ValidationError('不能创建admin用户')
        return attrs


