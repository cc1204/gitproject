# -*- coding: utf-8 -*-
from django.urls import include,path
from rest_framework.authtoken.views import obtain_auth_token
from user import views
from rest_framework.routers import SimpleRouter,DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token



# 自动生成路由方法，必须使用试图集
# router = SimpleRouter()  # 没有跟路由  /user/  无法识别
router = DefaultRouter()    # 有根路由
router.register(r'user',views.UserViewSet)   # 配置路由

urlpatterns = [
    path('index',views.index),
    path('login/',obtain_jwt_token),
    path('apiview/',views.UserInfoViewSet.as_view()),
    path('api-auth/',include('rest_framework.urls',namespace='rest_framework')), # 认证地址
    path('register/', views.RegisterView.as_view()), # 注册视图, /user/register/
    path('count/',views.RegCountView.as_view())   #  # 查询用户名手机号使用量的视图,
]

urlpatterns += router.urls  # 模块地址
