# -*- coding: utf-8 -*-

import os
from celery import Celery
import sys
from utils.rl_sms import send_message

# 定义celery实例, 需要的参数, 1, 实例名, 2, 任务发布位置, 3, 结果保存位置
app = Celery('mycelery',
             broker='redis://127.0.0.1:6379/14',  # 任务存放的地方
             backend='redis://127.0.0.1:6379/15')  # 结果存放的地方


@app.task
def add(x, y):
    return x + y


# celery项目中的所有导包地址, 都是以CELERY_BASE_DIR为基准设定.
# 执行celery命令时, 也需要进入CELERY_BASE_DIR目录执行.
CELERY_BASE_DIR = os.path.dirname(os.path.abspath(__file__))


@app.task(bind=True)
def send_sms_code(self, mobile, datas):
    sys.path.insert(0, os.path.join(CELERY_BASE_DIR, '../syl'))
    # 在方法中导包

    # time.sleep(5)
    try:
        # 用 res 接收发送结果, 成功是:０，　失败是：－１
        res = send_message(mobile, datas)
    except Exception as e:
        res = '-1'

    if res == '-1':
        # 如果发送结果是 -1  就重试.
        self.retry(countdown=5, max_retries=3, exc=Exception('短信发送失败'))